# Web Client requests
## Introduction
This document aims to provide information on how to use the Web Client in a context of a deployed MonALISA Proxy.
## Happy path
1. First necessary step is to connect to a proxy, via WebSockets.
For testing purposes, the alissandra cluster is deployed on alissandra01. Full WS link is: `ws://alissandra01.cern.ch:8080/web`.
2. Once the WS Connection is fully established, a client needs to register itself into the cluster. This can be done using the Registration message.
3. After that, the cluster is fully open to any valid request: discovery, subscription, or cache retrieval.
## JSON examples
### Registration message
Necessary information for registering a node: the nodeID (can be any random string, but we recommend it being something identifiable).
The trackId can also be any random string, but a common convention is for it to be the "<NODE_ID>-registration" for ease of debugging.
The `WEB_CLIENT` property is mandatory, please check the example below. This request will prompt no response
<details>
  <summary>Request</summary>

```JSON
{
    "trackId" : "asta-e-un-testing-node-registration",
    "node" : {
            "nodeId" : "asta-e-un-testing-node",
            "nodeUri" : "localhost",
            "capabilities" : [
                "WEB_CLIENT"
            ]
        },
    "type" : "registration-message-class"
}
```
</details>

<details>
  <summary>Response</summary>

No response!

</details>

### Discovery message
Besides information regarding the origin node and a trackId, an additional parameter for the depth of the tree level is required.
This level can be one of: `FARM`, `CLUSTER`, `NODE`, `MODULE`, `PARAMETER`. The response contains a single tree with fields populated until the desired level.
<details>
  <summary>Request</summary>

```JSON
{
    "messageTrackId" : "asta-e-un-testing-node-registration",
    "originNode" : {
            "nodeId" : "asta-e-un-testing-node",
            "nodeUri" : "localhost",
            "capabilities" : [
                "WEB_CLIENT"
            ]
        },
    "treeLevel" : "FARM",
    "type" : "discovery-message-class"
}
```

</details>

<details>
  <summary>Response</summary>

```JSON
{
  "type": "discovery-response-class",
  "messageTrackId": "asta-e-un-testing-node-registration",
  "originNode": {
    "nodeId": "alissandra05",
    "capabilities": [
      "FARM"
    ],
    "nodeURI": "128.142.249.113"
  },
  "destinationNode": {
    "nodeId": "asta-e-un-testing-node",
    "capabilities": [
      "WEB_CLIENT"
    ],
    "nodeURI": "localhost"
  },
  "response": [
    {
      "name": "alissandra05.cern.ch",
      "clusters": [
        {
          "name": "IPs",
          "nodes": [
            {
              "name": "localhost",
              "modules": [
                {
                  "name": "monIPAddresses"
                }
              ]
            }
          ]
        },
        {
          "name": "Machine",
          "nodes": [
            {
              "name": "CCISS1",
              "modules": [
                {
                  "name": "monStatusCmd"
                }
              ]
            },
            {
              "name": "System",
              "modules": [
                {
                  "name": "monStatusCmd"
                }
              ]
            },
            {
              "name": "disk",
              "modules": [
                {
                  "name": "monStatusCmd"
                }
              ]
            },
            {
              "name": "ipmi",
              "modules": [
                {
                  "name": "monStatusCmd"
                }
              ]
            },
            {
              "name": "kernel",
              "modules": [
                {
                  "name": "monStatusCmd"
                }
              ]
            },
            {
              "name": "memory",
              "modules": [
                {
                  "name": "monStatusCmd"
                }
              ]
            },
            {
              "name": "sensors",
              "modules": [
                {
                  "name": "monStatusCmd"
                }
              ]
            }
          ]
        },
        {
          "name": "Master",
          "nodes": [
            {
              "name": "alissandra05.cern.ch",
              "modules": [
                {
                  "name": "monXDRUDP"
                }
              ]
            },
            {
              "name": "localhost",
              "modules": [
                {
                  "name": "monProcIO"
                },
                {
                  "name": "monProcLoad"
                },
                {
                  "name": "monProcStat"
                }
              ]
            }
          ]
        },
        {
          "name": "MonaLisa",
          "nodes": [
            {
              "name": "localhost",
              "modules": [
                {
                  "name": "monMLStat"
                },
                {
                  "name": "monProcIO"
                },
                {
                  "name": "monProcLoad"
                },
                {
                  "name": "monProcStat"
                },
                {
                  "name": "mona"
                }
              ]
            }
          ]
        },
        {
          "name": "MonaLisa_LocalSysMon",
          "nodes": [
            {
              "name": "localhost",
              "modules": [
                {
                  "name": "monDiskIOStat"
                }
              ]
            }
          ]
        },
        {
          "name": "MonaLisa_ThPStat",
          "nodes": [
            {
              "name": "Monitor.Executor",
              "modules": [
                {
                  "name": "monThPStat"
                }
              ]
            },
            {
              "name": "Monitor.Network.Executor",
              "modules": [
                {
                  "name": "monThPStat"
                }
              ]
            },
            {
              "name": "Monitor.Store.Executor",
              "modules": [
                {
                  "name": "monThPStat"
                }
              ]
            },
            {
              "name": "Monitor.modules",
              "modules": [
                {
                  "name": "monThPStat"
                }
              ]
            }
          ]
        },
        {
          "name": "UPS",
          "nodes": [
            {
              "name": "localhost",
              "modules": [
                {
                  "name": "monAPCUPS"
                }
              ]
            }
          ]
        }
      ]
    }
  ]
}
```

</details>

### Subscribe request
A subscription consists of a request to continuously get live data, based on a defined predicate.
The `messageTrackId` is mandatory to be unique in the context of past messages, otherwise the subscription will not be validated in the backend.
The `addPredicates` is a list that contains the necessary predicates for the desired subscription.
Responses will come asynchronously and from all nodes, at once, each message containing results for a single node.
<details>
  <summary>Request</summary>

Subscribe request
```JSON
{
  "messageTrackId": "random-track-id",
  "originNode": {
    "nodeId": "asta-e-un-testing-node",
    "nodeUri": "localhost",
    "capabilities": [
      "WEB_CLIENT"
    ]
  },
  "addPredicates": [
    {
      "Farm": "*",
      "Cluster": "*",
      "Node": "*",
      "parameters" : ["*"],
      "tmin": 0,
      "tmax": -1
    }
  ],
  "deletePredicates": [],
  "type": "subscribe-message-class"
}
```

</details>

<details>
  <summary>Response</summary>

```JSON
{
  "type": "data-message-class",
  "originNode": {
    "nodeId": "alissandra01",
    "capabilities": [
      "FARM"
    ],
    "nodeURI": "128.142.249.109"
  },
  "data": [
    {
      "param_name": [
        "Load1",
        "Load5",
        "Load15",
        "Tasks_running",
        "Tasks_total"
      ],
      "param": [
        33.92,
        33.93,
        33.35,
        33,
        3749
      ],
      "time": 1718004496798,
      "NodeName": "localhost",
      "ClusterName": "Master",
      "FarmName": "alissandra01.cern.ch",
      "Module": "monProcLoad"
    }
  ],
  "destinationNode": {
    "nodeId": "asta-e-un-testing-node",
    "capabilities": [
      "WEB_CLIENT"
    ],
    "nodeURI": "localhost"
  },
  "messageTrackId": "random-track-id"
}
```

</details>


### Unsubscribe request
In order to unsubscribe to a previous request, the same `messageTrackId` used in the initial subscription request is necessary.
In order for the backend to understand that this is an unsubscribe request, both the `addPredicates` and `deletePredicates` lists should be empty.
<details>
  <summary>Request</summary>

```JSON
{
  "messageTrackId": "random-track-id",
  "originNode": {
    "nodeId": "asta-e-un-testing-node",
    "nodeUri": "localhost",
    "capabilities": [
      "WEB_CLIENT"
    ]
  },
  "addPredicates": [],
  "deletePredicates": [],
  "type": "subscribe-message-class"
}
```

</details>

<details>
  <summary>Response</summary>

No response!

</details>

### Cache Retrieval request
This request is very similar to a subscription request, but it will only drain the cache present in each farm.
Hence a single response per node of the cluster will come back.
The `predicates` fields serves the same purpose as the `addPredicates` from the subscription request.

<details>
  <summary>Request</summary>

```JSON
{
  "messageTrackId": "thunder-client-cache-retrieval",
  "originNode": {
    "nodeId": "thunder-client-cristi",
    "nodeUri": "localhost",
    "capabilities": [
      "WEB_CLIENT"
    ]
  },
  "predicates": [
    {
      "Farm": "*",
      "Cluster": "*",
      "Node": "*",
      "parameters": [
        "*"
      ],
      "tmin": 0,
      "tmax": 1719602751
    }
  ],
  "type": "cache-retrieval-message-class"
}
```

</details>

<details>
  <summary>Response</summary>

```JSON
{
  "type": "data-message-class",
  "originNode": {
    "nodeId": "alissandra01",
    "capabilities": [
      "FARM"
    ],
    "nodeURI": "128.142.249.109"
  },
  "data": [
    {
      "param_name": [
        "Load1"
      ],
      "param": [
        32.4
      ],
      "time": 1718605880498,
      "NodeName": "localhost",
      "ClusterName": "Master",
      "FarmName": "alissandra01.cern.ch",
      "Module": null,
      "min": 32.4,
      "max": 32.4
    },
    {
      "param_name": [
        "Load15"
      ],
      "param": [
        32.45
      ],
      "time": 1718605880498,
      "NodeName": "localhost",
      "ClusterName": "Master",
      "FarmName": "alissandra01.cern.ch",
      "Module": null,
      "min": 32.45,
      "max": 32.45
    },
    {
      "param_name": [
        "Load5"
      ],
      "param": [
        32.87
      ],
      "time": 1718605880498,
      "NodeName": "localhost",
      "ClusterName": "Master",
      "FarmName": "alissandra01.cern.ch",
      "Module": null,
      "min": 32.87,
      "max": 32.87
    },
    {
      "param_name": [
        "Tasks_running"
      ],
      "param": [
        37
      ],
      "time": 1718605880498,
      "NodeName": "localhost",
      "ClusterName": "Master",
      "FarmName": "alissandra01.cern.ch",
      "Module": null,
      "min": 37,
      "max": 37
    },
    {
      "param_name": [
        "Tasks_total"
      ],
      "param": [
        3548
      ],
      "time": 1718605880498,
      "NodeName": "localhost",
      "ClusterName": "Master",
      "FarmName": "alissandra01.cern.ch",
      "Module": null,
      "min": 3548,
      "max": 3548
    },
    {
      "param_name": [
        "TOTAL_NET"
      ],
      "param": [
        0
      ],
      "time": 1718605886514,
      "NodeName": "localhost",
      "ClusterName": "Master",
      "FarmName": "alissandra01.cern.ch",
      "Module": null,
      "min": 0,
      "max": 0
    },
    {
      "param_name": [
        "TOTAL_NET_IN"
      ],
      "param": [
        0
      ],
      "time": 1718605886514,
      "NodeName": "localhost",
      "ClusterName": "Master",
      "FarmName": "alissandra01.cern.ch",
      "Module": null,
      "min": 0,
      "max": 0
    },
    {
      "param_name": [
        "TOTAL_NET_OUT"
      ],
      "param": [
        0
      ],
      "time": 1718605886514,
      "NodeName": "localhost",
      "ClusterName": "Master",
      "FarmName": "alissandra01.cern.ch",
      "Module": null,
      "min": 0,
      "max": 0
    },
    {
      "param_name": [
        "br-5f3db5232a3d_COLLS"
      ],
      "param": [
        0
      ],
      "time": 1718605886514,
      "NodeName": "localhost",
      "ClusterName": "Master",
      "FarmName": "alissandra01.cern.ch",
      "Module": null,
      "min": 0,
      "max": 0
    },
    {
      "param_name": [
        "br-5f3db5232a3d_ERRS"
      ],
      "param": [
        0
      ],
      "time": 1718605886514,
      "NodeName": "localhost",
      "ClusterName": "Master",
      "FarmName": "alissandra01.cern.ch",
      "Module": null,
      "min": 0,
      "max": 0
    }
  ],
  "destinationNode": {
    "nodeId": "thunder-client-cristi",
    "capabilities": [
      "WEB_CLIENT"
    ],
    "nodeURI": "localhost"
  },
  "messageTrackId": "thunder-client-cache-retrieval"
}
```

</details>