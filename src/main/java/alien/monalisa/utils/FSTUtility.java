package alien.monalisa.utils;

import alien.monalisa.pojos.messages.BaseMessage;
import alien.monalisa.pojos.messages.DataMessage;
import jakarta.websocket.*;
import lia.Monitor.monitor.Result;
import org.nustaq.serialization.FSTConfiguration;

import java.nio.ByteBuffer;

public class FSTUtility implements Encoder.Binary<BaseMessage>, Decoder.Binary<BaseMessage> {
    static ThreadLocal<FSTConfiguration> FST = ThreadLocal.withInitial(FSTConfiguration::createDefaultConfiguration);

    @Override
    public BaseMessage decode(ByteBuffer bytes) throws DecodeException {
        return (BaseMessage) FST.get().asObject(bytes.array());
    }

    @Override
    public boolean willDecode(ByteBuffer bytes) {
        return true;
    }

    @Override
    public ByteBuffer encode(BaseMessage object) throws EncodeException {
        return ByteBuffer.wrap(FST.get().asByteArray(object));
    }

    @Override
    public void init(EndpointConfig endpointConfig) {
        FST.get().registerClass(BaseMessage.class);
        FST.get().registerClass(DataMessage.class);
        FST.get().registerClass(Result.class);
    }

    @Override
    public void destroy() {
    }
}
