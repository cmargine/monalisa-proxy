//package alien.monalisa.utils;
//
//import alien.monalisa.pojos.messages.*;
//import com.esotericsoftware.kryo.Kryo;
//import com.esotericsoftware.kryo.io.Input;
//import com.esotericsoftware.kryo.io.Output;
//import jakarta.websocket.Decoder;
//import jakarta.websocket.Encoder;
//import jakarta.websocket.EndpointConfig;
//
//import java.io.InputStream;
//import java.io.OutputStream;
//
//// NOT WORKING CURRENTLY, NEED TO CHECK WITH COSTIN
//public class KryoUtility implements Decoder.BinaryStream<BaseMessage>, Encoder.BinaryStream<BaseMessage> {
//
//    private final Kryo kryo;
//
//    public KryoUtility() {
//        // Initialize Kryo instance
//        kryo = new Kryo();
//        kryo.setRegistrationRequired(false);
//        // Configure Kryo (register classes, set up serializers, etc.) if needed
//        kryo.register(BaseMessage.class);
//        kryo.register(SingleDestinationMessage.class);
//        kryo.register(RegistrationMessage.class);
//        kryo.register(SubscribeMessage.class);
//        kryo.register(DataMessage.class);
//    }
//
//    @Override
//    public void encode(BaseMessage object, OutputStream os) {
//        // Serialize object using Kryo
//        Output output = new Output(os); // Writing directly to OutputStream
//        kryo.writeClassAndObject(output, object);
//        output.flush();
//        output.close();
//    }
//
//    @Override
//    public BaseMessage decode(InputStream is) {
//        // Deserialize binary message using Kryo
//        Input input = new Input(is); // Assuming InputStream contains the binary message
//        Object deserializedObject = kryo.readClassAndObject(input);
//        input.close();
//        if (deserializedObject instanceof BaseMessage) {
//            return (BaseMessage) deserializedObject;
//        } else {
//            throw new RuntimeException("Could not deserialize BaseMessage");
//        }
//    }
//
//    @Override
//    public void init(EndpointConfig config) {
//        // Initialization logic, if any
//    }
//
//    @Override
//    public void destroy() {
//        // Cleanup logic, if any
//    }
//}
