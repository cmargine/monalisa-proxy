package alien.monalisa.utils;

import alien.monalisa.pojos.messages.BaseMessage;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.websocket.*;
import lia.Monitor.monitor.monPredicate;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

public class JsonUtility implements Decoder.TextStream<BaseMessage>, Encoder.TextStream<BaseMessage> {
    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public BaseMessage decode(Reader reader) throws DecodeException, IOException {
        return mapper.readValue(reader, BaseMessage.class);
    }

    @Override
    public void encode(BaseMessage object, Writer writer) throws EncodeException, IOException {
        mapper.writeValue(writer, object);
    }

    @Override
    public void init(EndpointConfig endpointConfig) {
        mapper.addMixIn(monPredicate.class, MonPredicateMixIn.class);
    }

    @Override
    public void destroy() {
    }

    public abstract static class MonPredicateMixIn {

        // Constructor mapping
        @JsonCreator
        public MonPredicateMixIn(
                @JsonProperty("Farm") String farm,
                @JsonProperty("Cluster") String cluster,
                @JsonProperty("Node") String node,
                @JsonProperty("tmin") long tmin,
                @JsonProperty("tmax") long tmax,
                @JsonProperty("parameters") String[] parameters,
                @JsonProperty("constraints") String[] constraints
        ) {}

        // Field mapping for fields not included in the constructor
        @JsonProperty("id")
        private int id;

        @JsonProperty("bLastVals")
        private boolean bLastVals;
    }
}
