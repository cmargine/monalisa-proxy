package alien.monalisa.utils;

import java.util.Properties;

public class JavaPropertiesUtils {
    public static String getPropertyOrThrowError(Properties prop, String propKey) {
        final String res = prop.getProperty(propKey);
        if (res == null) {
            throw new IllegalStateException(String.format("Key %s not found in properties", propKey));
        } else {
            return res;
        }
    }
}
