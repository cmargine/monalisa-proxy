package alien.monalisa.pojos.messages;

import alien.monalisa.pojos.ml.MLClusterNode;
import alien.monalisa.pojos.ml.MLTreeLevel;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DiscoveryMessage extends BaseMessage {
    private static final long serialVersionUID = -4772285975695033920L;
    private final MLTreeLevel treeLevel;
    private final DiscoveryFilter filter;

    @JsonCreator
    public DiscoveryMessage(@JsonProperty("messageTrackId") String messageTrackId,
                            @JsonProperty("originNode") MLClusterNode originNode,
                            @JsonProperty("treeLevel") MLTreeLevel treeLevel,
                            @JsonProperty("filter") DiscoveryFilter filter) {
        super(messageTrackId, originNode);
        this.treeLevel = treeLevel;
        this.filter = filter;
    }

    public DiscoveryMessage(String messageTrackId, MLClusterNode originNode, MLTreeLevel treeLevel) {
        this(messageTrackId, originNode, treeLevel, allFilter());
    }

    public MLTreeLevel getTreeLevel() {
        return treeLevel;
    }

    public DiscoveryFilter getFilter() {
        return filter;
    }

    @Override
    public String toString() {
        return "DiscoveryMessage{" +
                "treeLevel=" + treeLevel +
                ", filter=" + filter +
                "} " + super.toString();
    }

    public static DiscoveryFilter allFilter() {
        return new DiscoveryFilter(".*", ".*", ".*", ".*", ".*");
    }

    public static class DiscoveryFilter {
        private final String farm;
        private final String cluster;
        private final String node;
        private final String module;
        private final String parameter;

        @JsonCreator
        public DiscoveryFilter(@JsonProperty("farm") String farm,
                               @JsonProperty("cluster") String cluster,
                               @JsonProperty("node") String node,
                               @JsonProperty("module") String module,
                               @JsonProperty("parameter") String parameter) {
            this.farm = farm;
            this.cluster = cluster;
            this.node = node;
            this.module = module;
            this.parameter = parameter;
        }

        public String getFarm() {
            return farm;
        }

        public String getCluster() {
            return cluster;
        }

        public String getNode() {
            return node;
        }

        public String getModule() {
            return module;
        }

        public String getParameter() {
            return parameter;
        }
    }
}
