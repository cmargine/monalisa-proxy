package alien.monalisa.pojos.messages;

import alien.monalisa.pojos.ml.MLClusterNode;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class DataMessage extends SingleDestinationMessage {
    private static final long serialVersionUID = -584165520310411509L;
    private final List<Object> data;

    public DataMessage(@JsonProperty("trackId") String trackId, @JsonProperty("originNode") MLClusterNode originNode,
                       @JsonProperty("data") List<Object> data, @JsonProperty("destinationNode") MLClusterNode destinationNode) {
        super(trackId, originNode, destinationNode);
        this.data = data;
    }

    public List<Object> getData() {
        return data;
    }

    @Override
    public String toString() {
        return "DataMessage{" +
                "data=" + data +
                "} " + super.toString();
    }
}
