package alien.monalisa.pojos.messages;

import alien.monalisa.pojos.ml.MLClusterNode;
import alien.monalisa.pojos.responses.DiscoveryResponse;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type"
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = DataMessage.class, name = "data-message-class"),
        @JsonSubTypes.Type(value = DiscoveryResponse.class, name = "discovery-response-class")
})
public abstract class SingleDestinationMessage extends BaseMessage {
    private final MLClusterNode destinationNode;

    @JsonCreator
    public SingleDestinationMessage(
            @JsonProperty("messageTrackId") String messageTrackId,
            @JsonProperty("originNode") MLClusterNode originNode,
            @JsonProperty("destinationNode") MLClusterNode destinationNode) {
        super(messageTrackId, originNode);
        this.destinationNode = destinationNode;
    }

    public MLClusterNode getDestinationNode() {
        return destinationNode;
    }

    @Override
    public String toString() {
        return "SingleDestinationMessage{" +
                "destinationNode=" + destinationNode +
                "} " + super.toString();
    }
}
