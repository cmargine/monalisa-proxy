package alien.monalisa.pojos.messages;

import alien.monalisa.pojos.ml.MLClusterNode;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class RegistrationMessage extends BaseMessage implements Serializable {
    private static final long serialVersionUID = 4731952979198308213L;
    @JsonCreator
    public RegistrationMessage(@JsonProperty("trackId") String trackId, @JsonProperty("node") MLClusterNode node) {
        super(trackId, node);
    }
}
