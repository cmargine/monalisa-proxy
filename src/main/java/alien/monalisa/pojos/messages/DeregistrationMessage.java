package alien.monalisa.pojos.messages;

import alien.monalisa.pojos.ml.MLClusterNode;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DeregistrationMessage extends BaseMessage {
    private static final long serialVersionUID = -9035222326599046094L;

    public DeregistrationMessage(@JsonProperty("trackId") String trackId, @JsonProperty("node") MLClusterNode node) {
        super(trackId, node);
    }
}
