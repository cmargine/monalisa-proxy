package alien.monalisa.pojos.messages;

import alien.monalisa.pojos.ml.MLClusterNode;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lia.Monitor.monitor.monPredicate;

import java.util.List;

public class CacheRetrievalMessage extends BaseMessage{
    private static final long serialVersionUID = 2484946434995370992L;
    private final List<monPredicate> predicates;

    @JsonCreator
    public CacheRetrievalMessage(
            @JsonProperty("messageTrackId") String messageTrackId,
            @JsonProperty("originNode") MLClusterNode originNode,
            @JsonProperty("predicates") List<monPredicate> predicates) {
        super(messageTrackId, originNode);
        this.predicates = predicates;
    }

    public List<monPredicate> getPredicates() {
        return predicates;
    }
}
