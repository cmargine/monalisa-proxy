package alien.monalisa.pojos.messages;

import alien.monalisa.pojos.ml.MLClusterNode;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lia.Monitor.monitor.monPredicate;

import java.util.List;
import java.util.Objects;

public class SubscribeMessage extends BaseMessage{
    private static final long serialVersionUID = 1473203530240433239L;
    private final List<monPredicate> addPredicates;
    private final List<monPredicate> deletePredicates;

    @JsonCreator
    public SubscribeMessage(
            @JsonProperty("messageTrackId") String messageTrackId,
            @JsonProperty("originNode") MLClusterNode originNode,
            @JsonProperty("addPredicates") List<monPredicate> addPredicates,
            @JsonProperty("deletePredicates") List<monPredicate> deletePredicates) {
        super(messageTrackId, originNode);
        this.addPredicates = addPredicates;
        this.deletePredicates = deletePredicates;
    }

    public List<monPredicate> getAddPredicates() {
        return addPredicates;
    }

    public List<monPredicate> getDeletePredicates() {
        return deletePredicates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubscribeMessage that = (SubscribeMessage) o;
        return Objects.equals(addPredicates, that.addPredicates) && Objects.equals(deletePredicates, that.deletePredicates);
    }

    @Override
    public int hashCode() {
        int result = Objects.hashCode(addPredicates);
        result = 31 * result + Objects.hashCode(deletePredicates);
        return result;
    }
}
