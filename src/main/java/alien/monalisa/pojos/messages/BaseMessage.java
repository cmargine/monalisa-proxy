package alien.monalisa.pojos.messages;

import alien.monalisa.pojos.ml.MLClusterNode;
import alien.monalisa.pojos.responses.DiscoveryResponse;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;
import java.util.Objects;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type"
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = RegistrationMessage.class, name = "registration-message-class"),
        @JsonSubTypes.Type(value = DeregistrationMessage.class, name = "deregistration-message-class"),
        @JsonSubTypes.Type(value = SingleDestinationMessage.class, name = "single-destination-message-class"),
        @JsonSubTypes.Type(value = SubscribeMessage.class, name = "subscribe-message-class"),
        @JsonSubTypes.Type(value = DiscoveryMessage.class, name = "discovery-message-class"),
        @JsonSubTypes.Type(value = CacheRetrievalMessage.class, name = "cache-retrieval-message-class"),
})
public abstract class BaseMessage implements Serializable {
    private static final long serialVersionUID = -8807362848774001985L;

    private final String messageTrackId;
    private final MLClusterNode originNode;

    @JsonCreator
    public BaseMessage(@JsonProperty("messageTrackId") String messageTrackId,
                       @JsonProperty("originNode") MLClusterNode originNode) {
        this.messageTrackId = messageTrackId;
        this.originNode = originNode;
    }

    public String getMessageTrackId() {
        return messageTrackId;
    }

    public MLClusterNode getOriginNode() {
        return originNode;
    }

    @Override
    public String toString() {
        return "BaseMessage{" +
                "messageTrackId='" + messageTrackId + '\'' +
                ", originNode=" + originNode +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseMessage that = (BaseMessage) o;
        return Objects.equals(messageTrackId, that.messageTrackId) && Objects.equals(originNode, that.originNode);
    }

    @Override
    public int hashCode() {
        int result = Objects.hashCode(messageTrackId);
        result = 31 * result + Objects.hashCode(originNode);
        return result;
    }
}
