package alien.monalisa.pojos.ml;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Set;

public class MLClusterNode implements Serializable {
    private static final long serialVersionUID = 5111685124753726242L;
    private final String nodeId;
    private final String nodeURI;
    public final Set<MLNodeCapabilities> capabilities;

    @JsonCreator
    public MLClusterNode(@JsonProperty("nodeId") String nodeId,
                         @JsonProperty("nodeUri") String nodeURI,
                         @JsonProperty("capabilities") Set<MLNodeCapabilities> capabilities) {
        this.nodeId = nodeId;
        this.nodeURI = nodeURI;
        this.capabilities = capabilities;
    }

    public String getNodeId() {
        return nodeId;
    }

    public String getNodeURI() {
        return nodeURI;
    }

    public Set<MLNodeCapabilities> getCapabilities() {
        return capabilities;
    }

    @Override
    public String toString() {
        return "MLClusterNode{" +
                "nodeId='" + nodeId + '\'' +
                ", nodeURI='" + nodeURI + '\'' +
                ", capabilities=" + capabilities +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MLClusterNode that = (MLClusterNode) o;
        return nodeId.equals(that.nodeId) && nodeURI.equals(that.nodeURI) && capabilities.equals(that.capabilities);
    }

    @Override
    public int hashCode() {
        int result = nodeId.hashCode();
        result = 31 * result + nodeURI.hashCode();
        result = 31 * result + capabilities.hashCode();
        return result;
    }
}
