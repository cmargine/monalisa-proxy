package alien.monalisa.pojos.ml;

public enum MLTreeLevel {
    FARM,
    CLUSTER,
    NODE,
    MODULE,
    PARAMETER
}
