package alien.monalisa.pojos.ml;

public enum MLNodeCapabilities {
    FARM(true, false),
    DATA_CACHE(true, false),
    PROXY(false, false),
    WEB_CLIENT(false, true),
    BINARY_CLIENT(false, true);

    private final boolean isDataGenerator;
    private final boolean isDataConsumer;

    MLNodeCapabilities(boolean isDataGenerator, boolean isDataConsumer) {
        this.isDataGenerator = isDataGenerator;
        this.isDataConsumer = isDataConsumer;
    }

    public boolean isDataGenerator() {
        return isDataGenerator;
    }

    public boolean isDataConsumer() {
        return isDataConsumer;
    }

    @Override
    public String toString() {
        return this.name();
    }
}
