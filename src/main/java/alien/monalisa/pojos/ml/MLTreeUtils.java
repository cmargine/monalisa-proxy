package alien.monalisa.pojos.ml;

import alien.monalisa.pojos.messages.DiscoveryMessage;
import lia.Monitor.monitor.MCluster;
import lia.Monitor.monitor.MFarm;
import lia.Monitor.monitor.MNode;

import java.util.List;
import java.util.regex.Pattern;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

public class MLTreeUtils {
    public static MLTree.Farm fromMFarm(MFarm mFarm, MLTreeLevel level) {
        switch (level) {
            case FARM:
                return new MLTree.Farm(mFarm.getName(), emptyList());
            case CLUSTER:
                return new MLTree.Farm(mFarm.getName(), createClusters(mFarm, false, false, false));
            case NODE:
                return new MLTree.Farm(mFarm.getName(), createClusters(mFarm, true, false, false));
            case MODULE:
                return new MLTree.Farm(mFarm.getName(), createClusters(mFarm, true, true, false));
            case PARAMETER:
                return new MLTree.Farm(mFarm.getName(), createClusters(mFarm, true, false, true));
            default:
                throw new RuntimeException("Invalid TreeRequestLevel");
        }
    }

    private static List<MLTree.Cluster> createClusters(MFarm mFarm, boolean includeNodes, boolean includeModules, boolean includeParameters) {
        return mFarm.getClusters().stream()
                .map(cluster -> new MLTree.Cluster(cluster.getName(),
                        includeNodes ? createNodes(cluster, includeModules, includeParameters) : emptyList()))
                .collect(toList());
    }

    private static List<MLTree.Node> createNodes(MCluster cluster, boolean includeModules, boolean includeParameters) {
        return cluster.getNodes().stream()
                .map(node -> new MLTree.Node(node.getName(),
                        includeModules ? createModules(node) : emptyList(),
                        includeParameters ? createParameters(node) : emptyList()))
                .collect(toList());
    }

    private static List<MLTree.Module> createModules(MNode node) {
        return node.getModuleList().stream()
                .map(MLTree.Module::new)
                .collect(toList());
    }

    private static List<MLTree.Parameter> createParameters(MNode node) {
        return node.getParameterList().stream()
                .map(MLTree.Parameter::new)
                .collect(toList());
    }

    public static MLTree.Farm filterBy(MLTree.Farm farm, DiscoveryMessage filter) {
        Pattern farmPattern = Pattern.compile(filter.getFilter().getFarm());

        if (!farmPattern.matcher(farm.getName()).matches()) {
            return null;
        }

        if (filter.getTreeLevel() == MLTreeLevel.FARM) {
            return new MLTree.Farm(farm.getName(), emptyList());
        }

        List<MLTree.Cluster> clusters = filterClusters(farm.getClusters(), filter);
        return new MLTree.Farm(farm.getName(), clusters);
    }

    private static List<MLTree.Cluster> filterClusters(List<MLTree.Cluster> clusters, DiscoveryMessage filter) {
        Pattern clusterPattern = Pattern.compile(filter.getFilter().getCluster());

        return clusters.stream()
                .filter(cluster -> clusterPattern.matcher(cluster.getName()).matches())
                .map(cluster -> filter.getTreeLevel() == MLTreeLevel.CLUSTER ?
                        new MLTree.Cluster(cluster.getName(), emptyList())
                        : new MLTree.Cluster(cluster.getName(), filterNodes(cluster.getNodes(), filter)))
                .collect(toList());
    }

    private static List<MLTree.Node> filterNodes(List<MLTree.Node> nodes, DiscoveryMessage filter) {
        Pattern nodePattern = Pattern.compile(filter.getFilter().getNode());

        return nodes.stream()
                .filter(node -> nodePattern.matcher(node.getName()).matches())
                .map(node -> filter.getTreeLevel() == MLTreeLevel.NODE ?
                        new MLTree.Node(node.getName(), emptyList(), emptyList())
                        : new MLTree.Node(node.getName(), filterModules(node.getModules(), filter), filterParameters(node.getParameters(), filter))
                )
                .collect(toList());
    }

    private static List<MLTree.Module> filterModules(List<MLTree.Module> modules, DiscoveryMessage filter) {
        if (filter.getTreeLevel() != MLTreeLevel.MODULE) {
            return emptyList();
        }

        Pattern modulePattern = Pattern.compile(filter.getFilter().getModule());

        return modules.stream()
                .filter(module -> modulePattern.matcher(module.getName()).matches())
                .collect(toList());
    }

    private static List<MLTree.Parameter> filterParameters(List<MLTree.Parameter> parameters, DiscoveryMessage filter) {
        if (filter.getTreeLevel() != MLTreeLevel.PARAMETER) {
            return emptyList();
        }

        Pattern parameterPattern = Pattern.compile(filter.getFilter().getParameter());

        return parameters.stream()
                .filter(param -> parameterPattern.matcher(param.getName()).matches())
                .collect(toList());
    }
}
