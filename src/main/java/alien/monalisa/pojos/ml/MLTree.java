package alien.monalisa.pojos.ml;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class MLTree {
    public static class Farm implements Serializable {
        final private String name;
        final private List<Cluster> clusters;

        @JsonCreator
        public Farm(@JsonProperty("name") String name,
                    @JsonProperty("clusters") List<Cluster> clusters) {
            this.name = name;
            this.clusters = clusters;
        }

        public String getName() {
            return name;
        }

        public List<Cluster> getClusters() {
            return clusters;
        }

        @Override
        public String toString() {
            return "Farm{" +
                    "name='" + name + '\'' +
                    ", clusters=" + clusters +
                    '}';
        }
    }

    public static class Cluster implements Serializable {
        final private String name;
        final private List<Node> nodes;

        @JsonCreator
        public Cluster(@JsonProperty("name") String name,
                       @JsonProperty("nodes") List<Node> nodes) {
            this.name = name;
            this.nodes = nodes;
        }

        public String getName() {
            return name;
        }

        public List<Node> getNodes() {
            return nodes;
        }

        @Override
        public String toString() {
            return "Cluster{" +
                    "name='" + name + '\'' +
                    ", nodes=" + nodes +
                    '}';
        }
    }

    public static class Node implements Serializable {
        final private String name;
        final private List<Module> modules;
        final private List<Parameter> parameters;

        @JsonCreator
        public Node(@JsonProperty("name") String name,
                    @JsonProperty("modules") List<Module> modules,
                    @JsonProperty("parameters") List<Parameter> parameters) {
            this.name = name;
            this.modules = modules;
            this.parameters = parameters;
        }

        public String getName() {
            return name;
        }

        public List<Module> getModules() {
            return modules;
        }

        public List<Parameter> getParameters() {
            return parameters;
        }

        @Override
        public String toString() {
            return "Node{" +
                    "name='" + name + '\'' +
                    ", modules=" + modules +
                    '}';
        }
    }

    public static class Module implements Serializable {
        final private String name;

        @JsonCreator
        public Module(@JsonProperty("name") String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return "Module{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }

    public static class Parameter implements Serializable {
        final private String name;

        @JsonCreator
        public Parameter(@JsonProperty("name") String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return "Parameter{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }
}
