package alien.monalisa.pojos.responses;

import alien.monalisa.pojos.messages.SingleDestinationMessage;
import alien.monalisa.pojos.ml.MLClusterNode;
import alien.monalisa.pojos.ml.MLTree;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class DiscoveryResponse extends SingleDestinationMessage {
    private final List<MLTree.Farm> response;

    @JsonCreator
    public DiscoveryResponse(@JsonProperty("messageTrackId") String messageTrackId,
                             @JsonProperty("originNode") MLClusterNode originNode,
                             @JsonProperty("destinationNode") MLClusterNode destinationNode,
                             @JsonProperty("response") List<MLTree.Farm> response) {
        super(messageTrackId, originNode, destinationNode);
        this.response = response;
    }

    public List<MLTree.Farm> getResponse() {
        return response;
    }

    @Override
    public String toString() {
        return "DiscoveryResponse{" +
                "response=" + response +
                "} " + super.toString();
    }
}
