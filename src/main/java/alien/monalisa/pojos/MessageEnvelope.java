package alien.monalisa.pojos;

import alien.monalisa.pojos.messages.BaseMessage;

import java.util.function.Consumer;

public class MessageEnvelope {
    private final BaseMessage message;
    private final Consumer<BaseMessage> onResponseHandler;

    public MessageEnvelope(BaseMessage message, Consumer<BaseMessage> onResponseHandler) {
        this.message = message;
        this.onResponseHandler = onResponseHandler;
    }

    public BaseMessage getMessage() {
        return message;
    }

    public Consumer<BaseMessage> getOnResponseHandler() {
        return onResponseHandler;
    }
}
