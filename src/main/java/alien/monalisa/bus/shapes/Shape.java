package alien.monalisa.bus.shapes;

import java.util.function.Function;

@FunctionalInterface
public interface Shape<T, V> {
    Function<T, V> getFunction();

    default <R> Function<T, R> andThen(Shape<V, R> shape) {
        return getFunction().andThen(shape.getFunction());
    }
}
