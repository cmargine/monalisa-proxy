package alien.monalisa.bus.shapes;

import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Collectors;

@FunctionalInterface
public interface OutputShape<R> extends Shape<R, R> {
    R write(R data);

    default Collection<R> write(Collection<R> c) {
        return c.stream()
                .map(this::write)
                .collect(Collectors.toList());
    }

    @Override
    default Function<R, R> getFunction() {
        return this::write;
    }
}
