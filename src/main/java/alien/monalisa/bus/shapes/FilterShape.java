package alien.monalisa.bus.shapes;

import java.util.Collection;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@FunctionalInterface
public interface FilterShape<T> extends Shape<T, T> {
    T filter(T data);

    default Collection<T> filter(Collection<T> c) {
        return c.stream()
                .map(this::filter)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Override
    default Function<T, T> getFunction() {
        return this::filter;
    }
}
