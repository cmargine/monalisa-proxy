package alien.monalisa.bus.shapes;

import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Collectors;

@FunctionalInterface
public interface MapShape<I, O> extends Shape<I, O> {
    O map(I data);

    default Collection<O> map(Collection<I> c) {
        return c.stream()
                .map(this::map)
                .collect(Collectors.toList());
    }

    @Override
    default Function<I, O> getFunction() {
        return this::map;
    }
}
