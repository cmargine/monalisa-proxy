package alien.monalisa.bus.shapes;

public class ShapeMetadata<T, V> {
    private final String name;
    private final Shape<T, V> shape;

    public ShapeMetadata(String name, Shape<T, V> shape) {
        this.name = name;
        this.shape = shape;
    }

    public String getName() {
        return name;
    }

    public Shape<T, V> getShape() {
        return shape;
    }
}
