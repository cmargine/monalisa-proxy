package alien.monalisa.bus;

import alien.monalisa.bus.shapes.Shape;
import alien.monalisa.bus.shapes.ShapeMetadata;
import lia.Monitor.monitor.Result;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.logging.Logger;

import static java.util.Collections.unmodifiableList;

public class Bus<T, R> implements Runnable {
    private final static Logger logger = Logger.getLogger(Bus.class.getName());
    private final String busId;
    private final Function<T, R> currentProcessingShapesFunction;
    private final List<ShapeMetadata<R, R>> outputShapesMetadata;
    private final BlockingQueue<T> busQueue;
    private boolean activateStatistics = false;
    private final Function<T, R> currentFinalFunction;
    private Boolean shouldStop = false;


    // statistics related fields
    public long itemsOffered = 0;
    public long itemsProcessed = 0;
    public long size = 0;

    public Bus(Function<T, R> processingFunctions, List<ShapeMetadata<R, R>> outputShapes,
               BlockingQueue<T> busQueue) {
        this(UUID.randomUUID().toString(), processingFunctions, outputShapes, busQueue);
    }

    public Bus(String busId, Function<T, R> processingFunctions, List<ShapeMetadata<R, R>> outputShapes,
               BlockingQueue<T> busQueue) {
        this(busId, processingFunctions, outputShapes, busQueue, true);
    }

    public Bus(String busId, Function<T, R> processingFunctions, List<ShapeMetadata<R, R>> outputShapes,
               BlockingQueue<T> busQueue, boolean activateStatistics) {
        this.busId = busId;
        this.currentProcessingShapesFunction = processingFunctions;
        this.outputShapesMetadata = unmodifiableList(outputShapes);
        this.busQueue = busQueue;
        this.activateStatistics = activateStatistics;
        this.currentFinalFunction = computeCurrentFunction(processingFunctions, outputShapes);
    }

    private Function<T, R> computeCurrentFunction(Function<T, R> currentProcessingShapesFunction,
                                                  List<ShapeMetadata<R, R>> outputShapes) {
        final Function<R, R> outputFunction;
        if (outputShapes.isEmpty()) {
            outputFunction = UnaryOperator.identity();
        } else {
            outputFunction = outputShapes.stream()
                    .map(ShapeMetadata::getShape)
                    .map(Shape::getFunction)
                    .reduce(Function::andThen)
                    .orElse(res -> {
                        logger.severe("No function to apply to the bus " + busId);
                        return null;
                    });
        };

        if (currentProcessingShapesFunction == null) {
            return (Function<T, R>) outputFunction;
        } else {
            return currentProcessingShapesFunction.andThen(outputFunction);
        }
    }

    public String getBusId() {
        return busId;
    }

    public boolean offer(T object) {
        if (object == null) {
            return false;
        }
        boolean res = busQueue.offer(object);
        if (res) {
            itemsOffered += 1;
        }
        return res;
    }

    public boolean offerAll(Collection<T> objects) {
        return objects.stream()
                .map(this::offer)
                .reduce(Boolean.TRUE, Boolean::logicalAnd);
    }

    @Override
    public void run() {
        final List<T> objs = new LinkedList<>();
        final Consumer<T> busConsumer = (t) -> {
            currentFinalFunction.apply(t);
            itemsProcessed += 1;
            if (t instanceof Result) {
                size += ((Result) t).param.length;
            }
        };

        if (activateStatistics) {
            Executors.newSingleThreadExecutor().execute(this::activatedStatisticsRun);
        }

        while (!shouldStop) {
            try {
                objs.add(busQueue.take());
                busQueue.drainTo(objs);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            } finally {
                objs.forEach(busConsumer);
                objs.clear();
            }
        }
    }

    public void stop() {
        shouldStop = true;
    }

    public Bus<T, R> addNewOutput(ShapeMetadata<R, R> output) {
        List<ShapeMetadata<R, R>> newOutputShapes = new LinkedList<>(outputShapesMetadata);
        newOutputShapes.add(output);
        return new Bus<>(busId, currentProcessingShapesFunction, newOutputShapes, busQueue, activateStatistics);
    }

    private void activatedStatisticsRun() {
        long lastTime = System.currentTimeMillis();
        long lastOffered = this.itemsOffered;
        long lastProcessed = this.itemsProcessed;
        long lastSize = this.size;

        while (true) {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            long currentTime = System.currentTimeMillis();
            long currentOffered = this.itemsOffered;
            long currentProcessed = this.itemsProcessed;
            long currentSize = this.size;
            double elapsedTimeInSeconds = (currentTime - lastTime) / 1000.0;
            double rateOffered = (currentOffered - lastOffered) / elapsedTimeInSeconds;
            double rateProcessed = (currentProcessed - lastProcessed) / elapsedTimeInSeconds;
            double rateSize = (currentSize - lastSize) / elapsedTimeInSeconds;
            lastTime = currentTime;
            lastOffered = currentOffered;
            lastProcessed = currentProcessed;
            lastSize = currentSize;

            logger.severe(busId + " Stats - Processed: " + currentProcessed + ", Offered: " + currentOffered + "\nProcessed Rate: " + String.format("%.2f", rateProcessed) + "/s, Offered Rate: " + String.format("%.2f", rateOffered) + "/s" + String.format(", Size rate: %.2f/s", rateSize) + "\n Queue size: " + busQueue.size());
        }
    }

    public static <T, V> ExecutorService runBusInSingleThread(Bus<T, V> bus) {
        final ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(bus);
        return executorService;
    }
}
