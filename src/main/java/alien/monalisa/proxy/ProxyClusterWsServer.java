package alien.monalisa.proxy;

import alien.monalisa.bus.Bus;
import alien.monalisa.pojos.MessageEnvelope;
import alien.monalisa.pojos.messages.BaseMessage;
import alien.monalisa.pojos.messages.DeregistrationMessage;
import alien.monalisa.pojos.messages.RegistrationMessage;
import alien.monalisa.pojos.messages.SingleDestinationMessage;
import alien.monalisa.pojos.ml.MLClusterNode;
import jakarta.websocket.*;
import jakarta.websocket.server.ServerEndpoint;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static alien.monalisa.bus.Bus.runBusInSingleThread;
import static alien.monalisa.pojos.ml.MLNodeCapabilities.FARM;
import static java.util.Collections.emptyList;

@ServerEndpoint(value = "/proxy")
public class ProxyClusterWsServer {
    private final static Logger logger = Logger.getLogger(ProxyClusterWsServer.class.getName());

    private final Bus<MessageEnvelope, Boolean> controllerBus;

    // TODO: SOLVE THIS ISSUE LATER
    // apparently a new instance of this class is created for each connection
    private static Bus<BaseMessage, Boolean> wsProxyServerBus;
    // apparently a new instance of this class is created for each connection
    private static final Map<MLClusterNode, Session> registeredNodes = new ConcurrentHashMap<>();

    public ProxyClusterWsServer(Bus<MessageEnvelope, Boolean> controllerBus) {
        this.controllerBus = controllerBus;

        synchronized (ProxyClusterWsServer.class) {
            if (wsProxyServerBus == null) {
                final Random r = new Random();
                final BlockingQueue<BaseMessage> outboundQueue = new ArrayBlockingQueue<>(1000000);
                wsProxyServerBus = new Bus<>("proxy-ws-server-bus-" + r.nextInt(), this::sendMessage, emptyList(), outboundQueue);
                runBusInSingleThread(wsProxyServerBus);
            }
        }
    }

    @OnOpen
    public void proxyOnOpen(Session session) {
        session.setMaxBinaryMessageBufferSize(8192 * 4);
        logger.info("Got a new incoming session");
    }

    @OnMessage
    public void proxyOnMessage(BaseMessage object, Session session) {
        // Register session if not registered
        if (object instanceof RegistrationMessage) {
            registeredNodes.put(object.getOriginNode(), session);
            logger.info("Registered cluster node: " + object.getOriginNode());
        }

        controllerBus.offer(new MessageEnvelope(object, wsProxyServerBus::offer));
    }

    @OnClose
    public void proxyOnClose(Session session, CloseReason reason) {
        final List<MLClusterNode> nodes = registeredNodes.entrySet().stream()
                .filter(entry -> entry.getValue() == session)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        if (nodes.isEmpty()) {
            logger.warning("Someone disconnected without being registered. Who was that?");
        } else if (nodes.size() > 1) {
            final StringBuilder loggerMessage = new StringBuilder();
            loggerMessage.append("Someone disconnected with more than one registered. How did we come to this?");
            loggerMessage.append("Here are the nodes:");
            for (MLClusterNode node : nodes) {
                loggerMessage.append(" ").append(node);
            }
            logger.warning(loggerMessage.toString());
        }

        for (MLClusterNode node : nodes) {
            registeredNodes.remove(node);
            controllerBus.offer(new MessageEnvelope(new DeregistrationMessage("deregistration-node-" + node.getNodeId(), node), wsProxyServerBus::offer));
            logger.info("Node disconnected: " + node);
            logger.info("Disconnection reason: " + reason);
        }
    }

    public Bus<BaseMessage, Boolean> getWsProxyServerBus() {
        return wsProxyServerBus;
    }

    private boolean sendMessage(BaseMessage message) {
        if (message instanceof SingleDestinationMessage) {
            final MLClusterNode destinationNode = ((SingleDestinationMessage) message).getDestinationNode();
            if (registeredNodes.containsKey(destinationNode)) {
                final Session session = registeredNodes.get(destinationNode);
                if (session == null) {
                    logger.warning("Somehow a registered node has a null session. How did we come to this?");
                } else {
                    try {
                        session.getAsyncRemote().sendObject(message);
//                        logger.info("Sent this message " + message + " to " + destinationNode);
                    } catch (Exception e) {
                        logger.warning("Could not send message to the cluster node");
                        return false;
                    }
                }
            } else {
                logger.warning("Got a SingleDestinationMessage to a target that is not registered: " + destinationNode);
                return false;
            }
        } else {
            registeredNodes.entrySet().stream()
                    .filter(entry -> entry.getKey().getCapabilities().contains(FARM))
                    .forEach(entry -> {
                        entry.getValue().getAsyncRemote().sendObject(message);
//                        logger.info("Sent this message " + message + " to " + entry.getKey());
                    });
        }
        return true;
    }
}
