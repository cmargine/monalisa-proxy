package alien.monalisa.proxy;

import alien.monalisa.bus.Bus;
import alien.monalisa.pojos.MessageEnvelope;
import alien.monalisa.pojos.messages.BaseMessage;
import alien.monalisa.pojos.messages.DeregistrationMessage;
import alien.monalisa.pojos.messages.RegistrationMessage;
import alien.monalisa.pojos.messages.SingleDestinationMessage;
import alien.monalisa.pojos.ml.MLClusterNode;
import jakarta.websocket.*;
import jakarta.websocket.server.ServerEndpoint;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static alien.monalisa.bus.Bus.runBusInSingleThread;
import static java.util.Collections.emptyList;

@ServerEndpoint(value = "/web")
public class ProxyWebWsServer {
    private final static Logger logger = Logger.getLogger(ProxyWebWsServer.class.getName());
    private final Bus<MessageEnvelope, Boolean> controllerBus;

    // TODO: SOLVE THIS ISSUE LATER
    // apparently a new instance of this class is created for each connection
    private static Bus<BaseMessage, Boolean> wsProxyWebServerBus;
    // apparently a new instance of this class is created for each connection
    private static final Map<MLClusterNode, Session> registeredClients = new ConcurrentHashMap<>();

    public ProxyWebWsServer(Bus<MessageEnvelope, Boolean> controllerBus) {
        this.controllerBus = controllerBus;
        synchronized (ProxyWebWsServer.class) {
            final BlockingQueue<BaseMessage> outboundQueue = new ArrayBlockingQueue<>(1000000);
            wsProxyWebServerBus = new Bus<>("proxy-ws-web-server-bus", this::sendMessage, emptyList(), outboundQueue);
            runBusInSingleThread(wsProxyWebServerBus);
        }
    }

    public Bus<BaseMessage, Boolean> getControllerBus() {
        return wsProxyWebServerBus;
    }

    @OnOpen
    public void proxyOnOpen(Session session) {
        session.setMaxTextMessageBufferSize(8192 * 8);
        logger.info("Got a new incoming session");
    }

    @OnMessage
    public void proxyOnMessage(BaseMessage object, Session session) {
        // Register session if not registered
        if (object instanceof RegistrationMessage) {
            registeredClients.put(object.getOriginNode(), session);
            logger.info("Registered cluster node: " + object.getOriginNode());
        }

        controllerBus.offer(new MessageEnvelope(object, wsProxyWebServerBus::offer));
    }

    @OnClose
    public void proxyOnClose(Session session, CloseReason reason) {
        final List<MLClusterNode> nodes = registeredClients.entrySet().stream()
                .filter(entry -> entry.getValue() == session)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        if (nodes.isEmpty()) {
            logger.warning("Someone disconnected without being registered. Who was that?");
        } else if (nodes.size() > 1) {
            final StringBuilder loggerMessage = new StringBuilder();
            loggerMessage.append("Someone disconnected with more than one registered. How did we come to this?");
            loggerMessage.append("Here are the nodes:");
            for (MLClusterNode node : nodes) {
                loggerMessage.append(" ").append(node);
            }
            logger.warning(loggerMessage.toString());
        }

        for (MLClusterNode node : nodes) {
            registeredClients.remove(node);
            controllerBus.offer(new MessageEnvelope(new DeregistrationMessage("deregistration-node-" + node.getNodeId(), node), wsProxyWebServerBus::offer));
            logger.info("Node disconnected: " + node);
            logger.info("Disconnection reason: " + reason);
        }
    }

    private boolean sendMessage(BaseMessage message) {
        if (message instanceof SingleDestinationMessage) {
            final MLClusterNode destinationNode = ((SingleDestinationMessage) message).getDestinationNode();
            if (registeredClients.containsKey(destinationNode)) {
                final Session session = registeredClients.get(destinationNode);
                if (session == null) {
                    logger.warning("Somehow a registered node has a null session. How did we come to this?");
                } else {
                    try {
                        session.getAsyncRemote().sendObject(message);
//                        logger.info("Sent this message " + message + " to " + destinationNode);
                        return true;
                    } catch (Exception e) {
                        logger.warning("Could not send message to the cluster node");
                        return false;
                    }
                }
            } else {
                logger.warning("Got a SingleDestinationMessage to a target that is not registered: " + destinationNode);
                return false;
            }
        }
        return false;
    }
}
