package alien.monalisa.proxy;

import alien.monalisa.bus.Bus;
import alien.monalisa.pojos.MessageEnvelope;
import alien.monalisa.pojos.messages.BaseMessage;
import alien.monalisa.pojos.ml.MLClusterNode;
import alien.monalisa.utils.FSTUtility;
import alien.monalisa.utils.JsonUtility;
import jakarta.servlet.ServletContextEvent;
import jakarta.websocket.DeploymentException;
import jakarta.websocket.HandshakeResponse;
import jakarta.websocket.server.HandshakeRequest;
import jakarta.websocket.server.ServerContainer;
import jakarta.websocket.server.ServerEndpointConfig;
import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.servlets.DefaultServlet;
import org.apache.catalina.startup.Tomcat;
import org.apache.tomcat.websocket.server.Constants;
import org.apache.tomcat.websocket.server.DefaultServerEndpointConfigurator;
import org.apache.tomcat.websocket.server.WsContextListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;

import static alien.monalisa.pojos.ml.MLNodeCapabilities.PROXY;
import static alien.monalisa.pojos.ml.MLNodeCapabilities.WEB_CLIENT;
import static alien.monalisa.proxy.ProxyKeyProperties.*;
import static alien.monalisa.utils.JavaPropertiesUtils.getPropertyOrThrowError;
import static java.lang.Integer.parseInt;

public class ProxyRunner implements Runnable {
    public static Logger logger = Logger.getLogger(ProxyRunner.class.getName());

    private Connector connector;
    private Integer wsPort;
    private MLClusterNode thisNode;
    private static Bus<MessageEnvelope, Boolean> controllerBus;
    private static Bus<BaseMessage, Boolean> wsProxyClusterBus;
    private static Bus<BaseMessage, Boolean> wsProxyWebBus;
    private static String webAppFolder;

    public ProxyRunner(Properties prop) {
        loadProperties(prop);
    }

    private void loadProperties(Properties prop) {
        final String thisNodeAddress = getPropertyOrThrowError(prop, PROXY_WS_BINDING_ADDRESS.getPropertyKey());
        this.wsPort = parseInt(getPropertyOrThrowError(prop, PROXY_WS_PORT.getPropertyKey()));
        webAppFolder = getPropertyOrThrowError(prop, PROXY_WEBAPP_LOCATION.getPropertyKey());

        connector = new Connector("org.apache.coyote.http11.Http11Nio2Protocol");
        connector.setProperty("address", thisNodeAddress);
        connector.setPort(this.wsPort);
        connector.setProperty("maxThreads", getPropertyOrThrowError(prop, PROXY_WS_CONNECTOR_MAX_THREADS.getPropertyKey()));
        connector.setProperty("maxConnections", getPropertyOrThrowError(prop, PROXY_WS_CONNECTOR_MAX_CONNECTIONS.getPropertyKey()));
        connector.setProperty("connectionTimeout", getPropertyOrThrowError(prop, PROXY_WS_CONNECTOR_CONNECTION_TIMEOUT.getPropertyKey()));
//        connector.setProperty("compression", getPropertyOrThrowError(prop, PROXY_WS_CONNECTOR_COMPRESSION.getPropertyKey()));
//        connector.setProperty("compressionMinSize", getPropertyOrThrowError(prop, PROXY_WS_CONNECTOR_COMPRESSION_MIN_SIZE.getPropertyKey()));
//        connector.setProperty("useSendFile", getPropertyOrThrowError(prop, PROXY_WS_CONNECTOR_USE_SEND_FILE.getPropertyKey()));

        final String thisNodeId = getPropertyOrThrowError(prop, PROXY_ID.getPropertyKey());
        this.thisNode = new MLClusterNode(thisNodeId, thisNodeAddress + ":" + this.wsPort, Set.of(PROXY, WEB_CLIENT));
    }

    @Override
    public void run() {
        Tomcat tomcat = new Tomcat();
        tomcat.setBaseDir(System.getProperty("java.io.tmpdir"));
        tomcat.setPort(this.wsPort);

        tomcat.getService().removeConnector(tomcat.getConnector()); // remove default connector

        tomcat.setConnector(this.connector);

        // Add an empty Tomcat context
        final Context ctx = tomcat.addContext("", new File(webAppFolder).getAbsolutePath());

        // Configure websocket context listener
        ctx.addApplicationListener(Config.class.getName());
        Tomcat.addServlet(ctx, "default", new DefaultServlet());
        ctx.addServletMappingDecoded("/", "default");

        try {
            tomcat.start();

            new Thread(() -> tomcat.getServer().await()).start();
        } catch (LifecycleException e) {
            throw new RuntimeException(e);
        }
    }

    public static class Config extends WsContextListener {
        @Override
        public void contextInitialized(ServletContextEvent sce) {
            super.contextInitialized(sce);
            ServerContainer sc = (ServerContainer) sce.getServletContext()
                    .getAttribute(Constants.SERVER_CONTAINER_SERVLET_CONTEXT_ATTRIBUTE);
            try {
                sc.addEndpoint(ServerEndpointConfig.Builder
                        .create(ProxyClusterWsServer.class, "/proxy")
                        .configurator(new ProxyClusterWsConfigurator(controllerBus))
                        .decoders(List.of(FSTUtility.class))
                        .encoders(List.of(FSTUtility.class))
                        .build()
                );

                sc.addEndpoint(ServerEndpointConfig.Builder
                        .create(ProxyWebWsServer.class, "/web")
                        .configurator(new ProxyWebWsConfigurator(controllerBus))
                        .decoders(List.of(JsonUtility.class))
                        .encoders(List.of(JsonUtility.class))
                        .build()
                );
            } catch (DeploymentException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    public static class ProxyClusterWsConfigurator extends DefaultServerEndpointConfigurator {
        private final Bus<MessageEnvelope, Boolean> controllerBus;

        public ProxyClusterWsConfigurator(Bus<MessageEnvelope, Boolean> controllerBus) {
            this.controllerBus = controllerBus;
        }

        @Override
        public ServerEndpointConfig.Configurator getContainerDefaultConfigurator() {
            return super.getContainerDefaultConfigurator();
        }

        public <T> T getEndpointInstance(Class<T> clazz) {
            final ProxyClusterWsServer proxyClusterWsServer = new ProxyClusterWsServer(controllerBus);
            wsProxyClusterBus = proxyClusterWsServer.getWsProxyServerBus();
            return (T) proxyClusterWsServer;
        }
    }

    public static class ProxyWebWsConfigurator extends DefaultServerEndpointConfigurator {
        private final Bus<MessageEnvelope, Boolean> controllerBus;

        public ProxyWebWsConfigurator(Bus<MessageEnvelope, Boolean> controllerBus) {
            this.controllerBus = controllerBus;
        }

        @Override
        public ServerEndpointConfig.Configurator getContainerDefaultConfigurator() {
            return super.getContainerDefaultConfigurator();
        }

        public <T> T getEndpointInstance(Class<T> clazz) {
            final ProxyWebWsServer proxyWebWsServer = new ProxyWebWsServer(controllerBus);
            wsProxyWebBus = proxyWebWsServer.getControllerBus();
            return (T) proxyWebWsServer;
        }
    }

    public static void main(String[] args) throws IOException {
        // proxy config file setup
        String proxyConfigFile = System.getProperty("PROXY_CONFIG_FILE");
        if (proxyConfigFile == null) {
            proxyConfigFile = "src/main/resources/default-proxy.properties";
            logger.severe("Proxy config file not found, reverting to default parameters!");
        }

        // proxy setup
        final Properties testingProxyProperties = new Properties();
        testingProxyProperties.load(new FileInputStream(proxyConfigFile));
        ProxyRunner proxyRunner = new ProxyRunner(testingProxyProperties);

        final ProxyController proxyController = new ProxyController(
                proxyRunner.thisNode,
                baseMessage -> wsProxyClusterBus.offer(baseMessage),
                baseMessage -> wsProxyWebBus.offer(baseMessage));
        controllerBus = proxyController.getProxyControllerBus();

        proxyRunner.run();
    }
}
