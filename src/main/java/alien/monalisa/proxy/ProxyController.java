package alien.monalisa.proxy;

import alien.monalisa.bus.Bus;
import alien.monalisa.bus.shapes.OutputShape;
import alien.monalisa.bus.shapes.ShapeMetadata;
import alien.monalisa.pojos.MessageEnvelope;
import alien.monalisa.pojos.messages.*;
import alien.monalisa.pojos.ml.MLClusterNode;
import alien.monalisa.pojos.ml.MLTree;
import alien.monalisa.pojos.ml.MLTreeUtils;
import alien.monalisa.pojos.responses.DiscoveryResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;
import java.util.logging.Logger;

import static alien.monalisa.bus.Bus.runBusInSingleThread;
import static alien.monalisa.pojos.ml.MLNodeCapabilities.WEB_CLIENT;
import static alien.monalisa.pojos.ml.MLTreeLevel.FARM;
import static java.util.Collections.emptyList;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toList;

public class ProxyController {
    private final static Logger logger = Logger.getLogger(ProxyController.class.getName());
    private final Consumer<BaseMessage> proxyClusterMessageSender;
    private final Consumer<BaseMessage> proxyWebMessageSender;
    private final MLClusterNode thisNode;

    private final Bus<MessageEnvelope, Boolean> proxyControllerBus;
    private final Bus<DataMessage, Boolean> cacheRetrievalBus;

    private final Map<BaseMessage, Bus<BaseMessage, BaseMessage>> internalBuses = new HashMap<>();
    private final Map<Bus<?, ?>, ExecutorService> internalExecutors = new HashMap<>();

    private final List<MLClusterNode> registeredNodes = new ArrayList<>();

    public ProxyController(MLClusterNode thisNode, Consumer<BaseMessage> proxyClusterMessageSender, Consumer<BaseMessage> proxyWebMessageSender) {
        this.thisNode = thisNode;

        final BlockingQueue<MessageEnvelope> outBoundQueue = new ArrayBlockingQueue<>(1000000);
        this.proxyControllerBus = new Bus<>("proxy-controller-bus", this::executeMessage, emptyList(), outBoundQueue);
        this.proxyClusterMessageSender = proxyClusterMessageSender;
        this.proxyWebMessageSender = proxyWebMessageSender;
        runBusInSingleThread(proxyControllerBus);

        final BlockingQueue<DataMessage> cacheRetrievalQueue = new ArrayBlockingQueue<>(1000000);
        this.cacheRetrievalBus = new Bus<>("cache-retrieval-controller-bus", this::executeCacheRetrievalDataMessage, emptyList(), cacheRetrievalQueue);
        runBusInSingleThread(cacheRetrievalBus);
    }

    public Bus<MessageEnvelope, Boolean> getProxyControllerBus() {
        return proxyControllerBus;
    }

    public boolean executeMessage(MessageEnvelope envelope) {
        // TODO: get rid of originNode after registration
        if (envelope.getMessage() instanceof RegistrationMessage) {
            return executeRegistrationRequest((RegistrationMessage) envelope.getMessage());
        } else if (envelope.getMessage() instanceof SubscribeMessage) {
            return executeSubscriptionRequest((SubscribeMessage) envelope.getMessage(), envelope.getOnResponseHandler());
        } else if (envelope.getMessage() instanceof DataMessage) {
            return executeDataRequest((DataMessage) envelope.getMessage());
        } else if (envelope.getMessage() instanceof DeregistrationMessage) {
            return executeDeregistrationRequest((DeregistrationMessage) envelope.getMessage());
        } else if (envelope.getMessage() instanceof DiscoveryMessage) {
            // TODO: debug PARAMETER LEVEL EMPTY
            return executeDiscoveryRequest((DiscoveryMessage) envelope.getMessage(), envelope.getOnResponseHandler());
        } else if (envelope.getMessage() instanceof DiscoveryResponse) {
            return executeDiscoveryResponse((DiscoveryResponse) envelope.getMessage());
        } else if (envelope.getMessage() instanceof CacheRetrievalMessage) {
            return executeCacheRetrievalRequest((CacheRetrievalMessage) envelope.getMessage(), envelope.getOnResponseHandler());
        }

        return false;
    }

    public boolean executeCacheRetrievalDataMessage(DataMessage message) {
        if (message.getDestinationNode().getCapabilities().contains(WEB_CLIENT)) {
            proxyWebMessageSender.accept(message);
        } else {
            proxyClusterMessageSender.accept(message);
        }
        return true;
    }

    // --------------------------------------------------------
    // ALL DOWN HERE CAN BE PUT INTO SEPARATE EXECUTION CLASSES

    public boolean executeRegistrationRequest(RegistrationMessage message) {
        if (registeredNodes.contains(message.getOriginNode())) {
            logger.warning("Node tried to register twice: " + message.getOriginNode());
            return false;
        }
        registeredNodes.add(message.getOriginNode());
        logger.info("Node: " + message.getOriginNode() + " registered successfully!");
        return true;
    }

    public boolean executeDeregistrationRequest(DeregistrationMessage message) {
        final MLClusterNode deregisteredNode = message.getOriginNode();
        if (!registeredNodes.contains(deregisteredNode)) {
            logger.warning("Unregistered node tried to deregister: " + deregisteredNode);
            return false;
        }
        registeredNodes.remove(deregisteredNode);

        // Deregister all active subscriptions
        List<BaseMessage> subscriptions = getSubscriptionsForNode(deregisteredNode);
        for (BaseMessage subMessage : subscriptions) {
            final SubscribeMessage unsubscribeMessage = new SubscribeMessage(subMessage.getMessageTrackId(), subMessage.getOriginNode(), emptyList(), emptyList());
            final Bus<BaseMessage, BaseMessage> busToBeDeregistered = internalBuses.get(subMessage);
            proxyClusterMessageSender.accept(unsubscribeMessage);
            internalBuses.remove(subMessage);

            busToBeDeregistered.stop();

            internalExecutors.get(busToBeDeregistered).shutdown();
            internalExecutors.remove(busToBeDeregistered);
        }
        return true;
    }

    public boolean executeSubscriptionRequest(SubscribeMessage message, Consumer<BaseMessage> onResponseHandler) {
        if (!registeredNodes.contains(message.getOriginNode())) {
            logger.warning("Unregistered node tried to subscribe: " + message.getOriginNode());
            return false;
        }
        if (internalBuses.containsKey(message)) {
            logger.warning("Node tried to subscribe with the same ID twice: " + message.getMessageTrackId());
            return false;
        }

        final OutputShape<BaseMessage> outputShape = data -> {
            onResponseHandler.accept(data);
            return data;
        };

        final ShapeMetadata<BaseMessage, BaseMessage> shapeMetadata = new ShapeMetadata<>(message.getMessageTrackId() + "-output", outputShape);
        final BlockingQueue<BaseMessage> newQueue = new ArrayBlockingQueue<>(1000000);
        final Bus<BaseMessage, BaseMessage> bus = new Bus<>(message.getMessageTrackId(), identity(), List.of(shapeMetadata), newQueue);

        internalBuses.put(message, bus);
        internalExecutors.put(bus, runBusInSingleThread(bus));
        proxyClusterMessageSender.accept(message);
        logger.info("Got a subscription request from " + message.getOriginNode());
        return true;
    }

    public boolean executeDataRequest(DataMessage message) {
        if (!registeredNodes.contains(message.getOriginNode())) {
            logger.warning("Unregistered node tried to send data: " + message.getOriginNode());
            return false;
        }
        final List<Bus<BaseMessage, BaseMessage>> resultedBus = getBusesForMessageId(message);

        if (resultedBus.isEmpty()) {
            // TODO: for now, cache retrieval are only supported by WEB CLIENTS
            executeCacheRetrievalDataMessage(message);
            //logger.warning("Got DataMessage without subscription: " + message.getMessageTrackId());
            return true;
        }

        if (resultedBus.size() > 1) {
            logger.warning("Too many buses registered for the same subscription: " + resultedBus.size());
            return false;
        }

        Bus<BaseMessage, BaseMessage> returnBus = resultedBus.get(0);
        returnBus.offer(message);
        return true;
    }

    public boolean executeDiscoveryRequest(DiscoveryMessage message, Consumer<BaseMessage> responseHandler) {
        if (!registeredNodes.contains(message.getOriginNode())) {
            logger.warning("Unregistered node tried to discover: " + message.getOriginNode());
            return false;
        }

        if (message.getTreeLevel().equals(FARM)) {
            List<MLTree.Farm> allFarms = registeredNodes.stream()
                    .map(node -> new MLTree.Farm(node.getNodeId(), emptyList()))
                    .map(farm -> MLTreeUtils.filterBy(farm, message))
                    .collect(toList());
            final DiscoveryResponse response = new DiscoveryResponse(message.getMessageTrackId(), thisNode, message.getOriginNode(), allFarms);
            responseHandler.accept(response);
        } else {
            proxyClusterMessageSender.accept(message);
        }

        return true;
    }

    public boolean executeDiscoveryResponse(DiscoveryResponse message) {
        if (!registeredNodes.contains(message.getOriginNode())) {
            logger.warning("Discovery response to unregistered node " + message.getOriginNode());
            return false;
        }
        if (message.getDestinationNode().getCapabilities().contains(WEB_CLIENT)) {
            proxyWebMessageSender.accept(message);
        } else {
            proxyClusterMessageSender.accept(message);
        }
        return true;
    }

    public boolean executeCacheRetrievalRequest(CacheRetrievalMessage message, Consumer<BaseMessage> onResponseHandler) {
        if (!registeredNodes.contains(message.getOriginNode())) {
            logger.warning("Cache retrieval response to unregistered node " + message.getOriginNode());
            return false;
        }

        logger.info("Got a cache retrieval request from " + message.getOriginNode());
        proxyClusterMessageSender.accept(message);
        logger.info("Forwarded the cache retrieval further to cluster nodes " + message.getMessageTrackId());
        return true;
    }

    private List<Bus<BaseMessage, BaseMessage>> getBusesForMessageId(BaseMessage message) {
        return internalBuses.entrySet().stream()
                .filter(kv -> kv.getKey().getMessageTrackId().equals(message.getMessageTrackId()))
                .map(Map.Entry::getValue)
                .collect(toList());
    }

    private List<BaseMessage> getSubscriptionsForNode(MLClusterNode node) {
        return internalBuses.keySet().stream()
                .filter(baseMessageBaseMessageBus -> baseMessageBaseMessageBus.getOriginNode().equals(node))
                .collect(toList());
    }
}
