package alien.monalisa.farm;

public enum FarmKeyProperties {
    PROXY_ENDPOINT("capability.farm.proxy_url"),
    FARM_ID("capability.farm.id"),
    FARM_URI("capability.farm.uri");

    private final String propertyKey;

    FarmKeyProperties(String propertyKey) {
        this.propertyKey = propertyKey;
    }

    public String getPropertyKey() {
        return propertyKey;
    }
}
