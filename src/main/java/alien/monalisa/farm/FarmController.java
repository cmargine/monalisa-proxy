package alien.monalisa.farm;

import alien.monalisa.bus.Bus;
import alien.monalisa.bus.shapes.MapShape;
import alien.monalisa.bus.shapes.OutputShape;
import alien.monalisa.bus.shapes.Shape;
import alien.monalisa.bus.shapes.ShapeMetadata;
import alien.monalisa.pojos.MessageEnvelope;
import alien.monalisa.pojos.messages.*;
import alien.monalisa.pojos.ml.MLClusterNode;
import alien.monalisa.pojos.ml.MLTree;
import alien.monalisa.pojos.responses.DiscoveryResponse;
import lia.Monitor.DataCache.Cache;
import lia.Monitor.DataCache.DataSelect;
import lia.Monitor.monitor.Result;
import lia.Monitor.monitor.monPredicate;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collector;

import static alien.monalisa.bus.Bus.runBusInSingleThread;
import static alien.monalisa.pojos.ml.MLTreeUtils.filterBy;
import static alien.monalisa.pojos.ml.MLTreeUtils.fromMFarm;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

public class FarmController {
    private final static Logger logger = Logger.getLogger(FarmController.class.getName());

    // internal Bus for processing Messages (from LEFT to CENTER on diagram)
    private final Bus<MessageEnvelope, Boolean> farmControllerBus;

    // internal Buses for subscriptions (CENTER on diagram)
    private final List<Bus<Object, BaseMessage>> internalSubscriptionBuses = new ArrayList<>();
    private final List<Bus<Object, BaseMessage>> internalCacheBuses = new ArrayList<>();

    private final Map<Bus<?, ?>, ExecutorService> internalExecutors = new HashMap<>();

    private final MLClusterNode thisNode;

    public FarmController(MLClusterNode thisNode) {
        this.thisNode = thisNode;

        final BlockingQueue<MessageEnvelope> outboundQueue = new ArrayBlockingQueue<>(1000000);
        this.farmControllerBus = new Bus<>("farm-controller-bus", this::executeMessage, emptyList(), outboundQueue);
        runBusInSingleThread(farmControllerBus);
    }

    public Bus<MessageEnvelope, Boolean> getFarmControllerBus() {
        return farmControllerBus;
    }

    public void inboundSubscriptionObject(Object obj) {
        for (Bus<Object, BaseMessage> bus : internalSubscriptionBuses) {
            bus.offer(obj);
        }
    }

    public void inboundCacheObject(Object obj) {
        for (Bus<Object, BaseMessage> bus : internalCacheBuses) {
            bus.offer(obj);
        }
    }

    public boolean executeMessage(MessageEnvelope envelope) {
        if (envelope.getMessage() instanceof SubscribeMessage) {
            return executeSubscriptionRequest((SubscribeMessage) envelope.getMessage(), envelope.getOnResponseHandler());
        } else if (envelope.getMessage() instanceof DiscoveryMessage) {
            return executeDiscoveryRequest((DiscoveryMessage) envelope.getMessage(), envelope.getOnResponseHandler());
        } else if (envelope.getMessage() instanceof CacheRetrievalMessage) {
            return executeCacheRetrievalRequest((CacheRetrievalMessage) envelope.getMessage(), envelope.getOnResponseHandler());
        }
        return false;
    }

    // --------------------------------------------------------
    // ALL DOWN HERE CAN BE PUT INTO SEPARATE EXECUTION CLASSES
    private boolean executeSubscriptionRequest(SubscribeMessage message, Consumer<BaseMessage> onResponseHandler) {
        // Check if it is as unsubscribe request
        if (isUnsubscribeRequest(message)) {
            List<Bus<Object, BaseMessage>> activeBuses = internalSubscriptionBuses.stream()
                    .filter(bus -> bus.getBusId().equals(message.getMessageTrackId()))
                    .collect(toList());
            logger.info("Got an unsubscription message for ID " + message.getMessageTrackId() + ". Will delete " + activeBuses.size() + " buses.");
            for (Bus<Object, BaseMessage> busToBeDeregistered : activeBuses) {
                internalSubscriptionBuses.remove(busToBeDeregistered);
                busToBeDeregistered.stop();

                internalExecutors.get(busToBeDeregistered).shutdown();
                internalExecutors.remove(busToBeDeregistered);
            }
            return true;
        }

        final Function<Object, BaseMessage> mapShape = getFinalMapShape(message);

        final OutputShape<BaseMessage> outputShape = data -> {
            onResponseHandler.accept(data);
//            logger.info("Sent data back (according to subscription to " + message.getOriginNode());
            return data;
        };

        final ShapeMetadata<BaseMessage, BaseMessage> shapeMetadata = new ShapeMetadata<>(message.getMessageTrackId()+ "-output", outputShape);
        final BlockingQueue<Object> newQueue = new ArrayBlockingQueue<>(1000000);

        final Bus<Object, BaseMessage> bus = new Bus<>(message.getMessageTrackId(), mapShape, List.of(shapeMetadata), newQueue);
        internalSubscriptionBuses.add(bus);
        internalExecutors.put(bus, runBusInSingleThread(bus));
        logger.info("Executed a subscription request " + message);
        return true;
    }

    private boolean executeDiscoveryRequest(DiscoveryMessage message, Consumer<BaseMessage> onResponseHandler) {
        logger.info("Got a discovery Request");
        final MLTree.Farm partialResponse = fromMFarm(Cache.getMFarm(), message.getTreeLevel());
        logger.info("Here is the response before filtering for level: " + partialResponse);
        final MLTree.Farm response = filterBy(partialResponse, message);
        logger.info("Here is the response after filtering for level: " + response);
        if (response != null) {
            onResponseHandler.accept(new DiscoveryResponse(message.getMessageTrackId(), thisNode, message.getOriginNode(), List.of(response)));
            return true;
        }
        return false;
    }

    private boolean executeCacheRetrievalRequest(CacheRetrievalMessage message, Consumer<BaseMessage> onResponseHandler) {
        final Cache cache = Cache.getInstance();
        List<monPredicate> allPredicates = message.getPredicates();

        // TODO: this is not truly async in this context, should be executed inside a bus
        allPredicates.stream()
                .map(cache::select)
                .flatMap(Collection::stream)
                .collect(groupingBySize(10))
                .stream()
                .map(resList -> new DataMessage(message.getMessageTrackId(), thisNode, resList, message.getOriginNode()))
                .forEach(onResponseHandler);

        return true;
    }

    private static boolean isUnsubscribeRequest(SubscribeMessage message) {
        return message.getAddPredicates().isEmpty() && message.getDeletePredicates().isEmpty();
    }

    private Function<Object, BaseMessage> getFinalMapShape(SubscribeMessage message) {
        Shape<Object, Object> currentShape = null;
        if (!message.getAddPredicates().isEmpty()) {
            for (monPredicate pred : message.getAddPredicates()) {
                MapShape<Object, Object> newMapShape;
                newMapShape = data -> {
//                    logger.info("Starting monPredicate for " + data);
                    if (data instanceof Result) {
                        Result result = DataSelect.matchResult((Result) data, pred);
//                        logger.info("DataSelect result is " + result);
                        return result;
                    }
                    // only Results are permitted when sending monPredicates
//                    logger.info("Returning null :(");
                    return null;
                };
                if (currentShape == null) {
                    currentShape = newMapShape;
                } else {
                    currentShape.andThen(newMapShape);
                }
            }
        }

        final MapShape<Object, BaseMessage> mapShape = data ->
        {
            if (data != null) {
                return new DataMessage(message.getMessageTrackId(), thisNode, List.of(data), message.getOriginNode());
            }
            return null;
        };

        return currentShape != null ? currentShape.andThen(mapShape) : mapShape.getFunction();
    }

    // Custom collector to group elements into batches of a specific size
    public static <T> Collector<T, List<List<T>>, List<List<T>>> groupingBySize(int size) {
        return Collector.of(
                ArrayList::new,
                (list, item) -> {
                    if (list.isEmpty() || list.get(list.size() - 1).size() == size) {
                        list.add(new ArrayList<>());
                    }
                    list.get(list.size() - 1).add(item);
                },
                (left, right) -> {
                    left.addAll(right);
                    return left;
                }
        );
    }
}
