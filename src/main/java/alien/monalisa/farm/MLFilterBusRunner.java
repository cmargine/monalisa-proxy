package alien.monalisa.farm;

import lia.Monitor.Filters.GenericMLFilter;
import lia.Monitor.monitor.monPredicate;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;

public class MLFilterBusRunner extends GenericMLFilter {
    private final static Logger logger = Logger.getLogger(MLFilterBusRunner.class.getName());
    private final FarmRunner runner;
    public MLFilterBusRunner(String farmName) {
        super(farmName);
        this.farmName = farmName;

        /* START FARM SETUP */
        // farm config file setup
        String farmConfigFile = System.getProperty("FARM_CONFIG_FILE");
        if (farmConfigFile == null) {
            throw new RuntimeException("FARM_CONFIG_FILE not set");
        }

        // farm setup
        final Properties testingFarmProperties = new Properties();
        try {
            testingFarmProperties.load(new FileInputStream(farmConfigFile));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        this.runner = new FarmRunner(testingFarmProperties);
        runner.run();
        /* END FARM SETUP */

        logger.info("Started the MLFilterBusRunner");
    }
    @Override
    public String getName(){
        return "FilterBus";
    }

    @Override
    public long getSleepTime () {
        return 60000;
    }

    @Override
    public monPredicate[] getFilterPred () {
        return null;
    }

    @Override
    public void notifyResult (Object o) {
        offerController(o);
    }

    public Object expressResults () {
        return null;
    }

    public void offerController (Object o) {
        if (o == null) {
            return;
        }

        runner.getController().inboundSubscriptionObject(o);
    }
}
