package alien.monalisa.farm;

import alien.monalisa.pojos.messages.RegistrationMessage;
import alien.monalisa.pojos.ml.MLClusterNode;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.*;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static alien.monalisa.farm.FarmKeyProperties.*;
import static alien.monalisa.pojos.ml.MLNodeCapabilities.FARM;
import static alien.monalisa.utils.JavaPropertiesUtils.getPropertyOrThrowError;
import static java.util.stream.Collectors.toList;

public class FarmRunner implements Runnable {
    private final static Logger logger = Logger.getLogger(FarmRunner.class.getName());
    private InetAddress[] proxyEndpoints;
    private MLClusterNode thisNode;

    private FarmController controller;
    private final Map<InetAddress, FarmWsClient> senders = new HashMap<>();

    public FarmRunner(Properties prop) {
        loadProperties(prop);
    }

    public void loadProperties(Properties prop) {
        final String proxyUrlValue = getPropertyOrThrowError(prop, PROXY_ENDPOINT.getPropertyKey());
        final String farmId = getPropertyOrThrowError(prop, FARM_ID.getPropertyKey());
        final String farmUri = getPropertyOrThrowError(prop, FARM_URI.getPropertyKey());

        try {
            new URI(farmUri);
        } catch (URISyntaxException e) {
            throw new RuntimeException("Invalid local farm URI: " + farmUri, e);
        }

        try {
            proxyEndpoints = InetAddress.getAllByName(proxyUrlValue);
            logger.info("Got the following proxy endpoints: " + Arrays.stream(proxyEndpoints).map(InetAddress::getHostAddress).collect(toList()));
        } catch (UnknownHostException e) {
            throw new RuntimeException("Invalid proxy URL: " + proxyUrlValue, e);
        }

        this.thisNode = new MLClusterNode(farmId, farmUri, Set.of(FARM));
        // TODO: change capabilities to be configurable when adding the Cache Capability
    }

    @Override
    public void run() {
        // set up the controller
        this.controller = new FarmController(thisNode);

        for (InetAddress address : proxyEndpoints) {
            logger.info("Attempting to connect to " + address.getHostAddress());

            try {
                if (isSameServer(senders.keySet(), address)) {
                    continue;
                }

                String hostAddress = address.getHostAddress();
                String uriString = address instanceof Inet6Address
                        ? "ws://[" + hostAddress + "]:8080/proxy"
                        : "ws://" + hostAddress + ":8080/proxy";
                final URI uri = new URI(uriString);
                final FarmWsClient client = new FarmWsClient(uri, this.controller.getFarmControllerBus());

                final String trackId = "initial-registration-" + thisNode.getNodeId();
                final RegistrationMessage initialMessage = new RegistrationMessage(trackId, thisNode);
                client.getWsProxyClientBus().offer(initialMessage);

                senders.put(address, client);
            } catch (URISyntaxException e) {
                logger.severe("Invalid proxy IP address: " + address);
            } catch (IllegalStateException e) {
                logger.severe("Could not instantiate proxy endpoint: " + address);
            }
        }

        logger.severe("FarmRunner is running");
        logger.severe("Connected to the following proxies: " +
                senders.keySet().stream()
                        .map(InetAddress::getCanonicalHostName)
                        .collect(Collectors.toList())
        );
    }

    public FarmController getController() {
        return controller;
    }

    public static void main(String[] args) throws IOException {
        // proxy config file setup
        String farmConfigFile = System.getProperty("FARM_CONFIG_FILE");
        if (farmConfigFile == null) {
            farmConfigFile = "src/main/resources/default-farm.properties";
        }

        // proxy setup
        final Properties testingFarmProperties = new Properties();
        testingFarmProperties.load(new FileInputStream(farmConfigFile));
        FarmRunner farmRunner = new FarmRunner(testingFarmProperties);

        farmRunner.run();
    }

    private boolean isSameServer(Collection<InetAddress> connectedIps, InetAddress newIp) {
        String newIpHostName = newIp.getCanonicalHostName();
        for (InetAddress connectedIp : connectedIps) {
            if (connectedIp.getCanonicalHostName().equals(newIpHostName)) {
                final String message = "Attempted to connect to %s using %s but there is already a connection using %s. Skipping...";
                logger.severe(String.format(message, newIpHostName, newIp.getHostAddress(), connectedIp.getHostAddress()));
                return true;
            }
        }
        return false;
    }
}



