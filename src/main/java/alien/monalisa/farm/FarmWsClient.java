package alien.monalisa.farm;

import alien.monalisa.bus.Bus;
import alien.monalisa.pojos.MessageEnvelope;
import alien.monalisa.pojos.messages.BaseMessage;
import alien.monalisa.utils.FSTUtility;
import jakarta.websocket.*;

import java.net.URI;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import static alien.monalisa.bus.Bus.runBusInSingleThread;
import static java.util.Collections.emptyList;

@ClientEndpoint(encoders = FSTUtility.class, decoders = FSTUtility.class)
public class FarmWsClient {
    private final static Logger logger = Logger.getLogger(FarmWsClient.class.getName());

    private final Bus<BaseMessage, Boolean> wsProxyClientBus;
    private final Bus<MessageEnvelope, Boolean> controllerBus;

    private Session sessionWithProxy = null;

    public FarmWsClient(URI proxyEndpoint, Bus<MessageEnvelope, Boolean> controllerBus) {
        this.controllerBus = controllerBus;
        try {
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            container.connectToServer(this, proxyEndpoint);
        } catch (Exception e) {
            logger.warning("Could not connect to the proxy, the FarmWSClient will not work properly. Exception: " + e);
            throw new IllegalStateException(e);
        }

        final Random r = new Random();
        final BlockingQueue<BaseMessage> outboundQueue = new ArrayBlockingQueue<>(1000000);
        wsProxyClientBus = new Bus<>("farm-ws-proxy-client-bus-" + r.nextInt(), this::sendMessage, emptyList(), outboundQueue);
        runBusInSingleThread(wsProxyClientBus);
    }

    @OnOpen
    public void farmOnOpen(Session session) {
        session.setMaxBinaryMessageBufferSize(8192 * 4);
        this.sessionWithProxy = session;
    }

    @OnMessage
    public void farmOnMessage(BaseMessage object, Session session) {
//        logger.info("Got this message " + object);
        controllerBus.offer(new MessageEnvelope(object, wsProxyClientBus::offer));
    }

    @OnClose
    public void proxyOnClose(Session session, CloseReason reason) {
        logger.severe("Connection closed with Proxy");
        logger.severe("Disconnection reason: " + reason);
    }

    public Bus<BaseMessage, Boolean> getWsProxyClientBus() {
        return wsProxyClientBus;
    }

    private boolean sendMessage(BaseMessage object) {
        if (sessionWithProxy != null && sessionWithProxy.isOpen()) {
            try {
                this.sessionWithProxy.getAsyncRemote().sendObject(object);
//                logger.info("Sent this message " + object + " back to proxy!");
                return true;
            } catch (Exception e) {
                // TODO: when sending objects with .getAsyncRemote(), the exceptions are not logged, need to figure out why
                logger.log(Level.SEVERE, "Could not send message to the Proxy {0}", e);
                return false;
            }
        } else {
            return false;
        }
    }
}
