package alien.monalisa.sub;

public enum SubKeyProperties {
    PROXY_ENDPOINT("capability.subscriber.proxy_url"),
    SUBSCRIBER_NAME("capability.subscriber.name"),
    SUBSCRIBER_IP("capability.subscriber.ip"),
    SUBSCRIBER_HOSTNAME("capability.subscriber.hostname"),
    SUBSCRIBER_FARM_PATTERN("capability.subscriber.farm_pattern"),
    SUBSCRIBER_MODULE_PATTERN("capability.subscriber.module_pattern");
    private final String propertyKey;
    SubKeyProperties(String propertyKey) {
        this.propertyKey = propertyKey;
    }
    public String getPropertyKey() {
        return propertyKey;
    }
}
