package alien.monalisa.sub;

import alien.monalisa.bus.Bus;
import alien.monalisa.pojos.MessageEnvelope;
import alien.monalisa.pojos.messages.BaseMessage;
import alien.monalisa.utils.FSTUtility;
import jakarta.websocket.*;

import java.net.URI;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Logger;

import static alien.monalisa.bus.Bus.runBusInSingleThread;
import static java.util.Collections.emptyList;

@ClientEndpoint(encoders = FSTUtility.class, decoders = FSTUtility.class)
public class SubWsClient {
    private final static Logger logger = Logger.getLogger(SubWsClient.class.getName());

    private final Bus<BaseMessage, Boolean> wsProxyClientBus;
    private final Bus<MessageEnvelope, Boolean> controllerBus;

    private Session session = null;

    public SubWsClient(URI proxyEndpoint, Bus<MessageEnvelope, Boolean> controllerBus) {
        this.controllerBus = controllerBus;

        try {
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            container.connectToServer(this, proxyEndpoint);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        final BlockingQueue<BaseMessage> outboundQueue = new ArrayBlockingQueue<>(1000000);
        wsProxyClientBus = new Bus<>("client-ws-proxy-client-bus", this::sendMessage, emptyList(), outboundQueue);
        runBusInSingleThread(wsProxyClientBus);
    }

    @OnOpen
    public void subOnOpen(Session session) {
        session.setMaxTextMessageBufferSize(8192 * 8);
        this.session = session;
    }

    @OnMessage
    public void subOnMessage(BaseMessage object, Session session) {
        controllerBus.offer(new MessageEnvelope(object, wsProxyClientBus::offer));
    }

    @OnClose
    public void proxyOnClose(Session session, CloseReason reason) {
        logger.severe("Connection closed with Proxy");
    }

    public Bus<BaseMessage, Boolean> getWsProxyClientBus() {
        return wsProxyClientBus;
    }

    private boolean sendMessage(BaseMessage object) {
        if (session != null && session.isOpen()) {
            try {
                session.getAsyncRemote().sendObject(object);
                return true;
            } catch (Exception e) {
                logger.severe("Could not send message to the Proxy with error" + e.getMessage());
                return false;
            }
        } else {
            return false;
        }
    }
}
