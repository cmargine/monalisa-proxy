package alien.monalisa.sub;

import alien.monalisa.bus.Bus;
import alien.monalisa.bus.shapes.OutputShape;
import alien.monalisa.bus.shapes.ShapeMetadata;
import alien.monalisa.pojos.MessageEnvelope;
import alien.monalisa.pojos.messages.*;
import alien.monalisa.pojos.ml.MLClusterNode;
import alien.monalisa.pojos.responses.DiscoveryResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.function.Consumer;
import java.util.logging.Logger;

import static alien.monalisa.bus.Bus.runBusInSingleThread;
import static java.util.Collections.emptyList;
import static java.util.function.Function.identity;

public class SubController {
    private final static Logger logger = Logger.getLogger(SubController.class.getName());

    private final Bus<MessageEnvelope, Boolean> controllerBus;

    private final Map<String, Bus<BaseMessage, BaseMessage>> internalBuses = new HashMap<>();

    private final Consumer<BaseMessage> proxyClusterMessageSender;

    private final MLClusterNode thisNode;

    public SubController(MLClusterNode thisNode, Consumer<BaseMessage> proxyClusterMessageSender) {
        this.thisNode = thisNode;
        this.proxyClusterMessageSender = proxyClusterMessageSender;

        final BlockingQueue<MessageEnvelope> outboundQueue = new ArrayBlockingQueue<>(1000000);
        this.controllerBus = new Bus<>("client-controller-bus", this::executeMessage, emptyList(), outboundQueue);
        runBusInSingleThread(controllerBus);
    }

    public void inboundMessage(MessageEnvelope message) {
        controllerBus.offer(message);
    }

    public Bus<MessageEnvelope, Boolean> getControllerBus() {
        return controllerBus;
    }

    public boolean executeMessage(MessageEnvelope envelope) {
        if (envelope.getMessage() instanceof RegistrationMessage) {
            return executeRegistrationRequest((RegistrationMessage) envelope.getMessage());
        } else if (envelope.getMessage() instanceof SubscribeMessage) {
            return executeSubscribeRequest((SubscribeMessage) envelope.getMessage(), envelope.getOnResponseHandler());
        } else if (envelope.getMessage() instanceof DataMessage) {
            return executeDataRequest((DataMessage) envelope.getMessage());
        } else if (envelope.getMessage() instanceof DiscoveryMessage) {
            return executeDiscoveryRequest((DiscoveryMessage) envelope.getMessage());
        } else if (envelope.getMessage() instanceof DiscoveryResponse) {
            return executeDiscoveryResponse((DiscoveryResponse) envelope.getMessage());
        }
        return false;
    }

    // --------------------------------------------------------
    // ALL DOWN HERE CAN BE PUT INTO SEPARATE EXECUTION CLASSES

    private boolean executeRegistrationRequest(RegistrationMessage message) {
        if (!message.getOriginNode().equals(thisNode)) {
            logger.warning("Why do you want to execute a registration request for a different node?");
            return false;
        }
        proxyClusterMessageSender.accept(message);
        logger.info("Registered with proxy");
        return true;
    }

    private boolean executeDiscoveryRequest(DiscoveryMessage message) {
        if (!message.getOriginNode().equals(thisNode)) {
            logger.warning("Why do you want to execute a registration request for a different node?");
            return false;
        }
        proxyClusterMessageSender.accept(message);
        return true;
    }

    private boolean executeSubscribeRequest(SubscribeMessage message, Consumer<BaseMessage> onResponseHandler) {
        final OutputShape<BaseMessage> outputShape = data -> {
            onResponseHandler.accept(data);
            return data;
        };

        final ShapeMetadata<BaseMessage, BaseMessage> shapeMetadata = new ShapeMetadata<>(message.getMessageTrackId() + "-output", outputShape);
        final BlockingQueue<BaseMessage> newQueue = new ArrayBlockingQueue<>(1000000);
        final Bus<BaseMessage, BaseMessage> bus = new Bus<>(message.getMessageTrackId(), identity(), List.of(shapeMetadata), newQueue);

        internalBuses.put(message.getMessageTrackId(), bus);
        runBusInSingleThread(bus);
        proxyClusterMessageSender.accept(message);
        logger.info("Sent to proxy a subscribe request");
        return true;
    }

    private boolean executeDataRequest(DataMessage message) {
        if (!internalBuses.containsKey(message.getMessageTrackId())) {
            logger.warning("Got DataMessage without subscription: " + message.getMessageTrackId());
            return false;
        }

        Bus<BaseMessage, BaseMessage> returnBus = internalBuses.get(message.getMessageTrackId());
        returnBus.offer(message);
        return true;
    }

    private boolean executeDiscoveryResponse(DiscoveryResponse message) {
        logger.info("Got a response for my Discovery request!");
        logger.info(message.toString());
        logger.info(message.getResponse().toString());

        return true;
    }
}
