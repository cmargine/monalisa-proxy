package alien.monalisa.sub;

import alien.monalisa.pojos.MessageEnvelope;
import alien.monalisa.pojos.messages.BaseMessage;
import alien.monalisa.pojos.messages.DiscoveryMessage;
import alien.monalisa.pojos.messages.RegistrationMessage;
import alien.monalisa.pojos.messages.SubscribeMessage;
import alien.monalisa.pojos.ml.MLClusterNode;
import alien.monalisa.pojos.ml.MLTreeLevel;
import lia.Monitor.monitor.monPredicate;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.logging.Logger;

import static alien.monalisa.pojos.ml.MLNodeCapabilities.BINARY_CLIENT;
import static alien.monalisa.sub.SubKeyProperties.*;
import static alien.monalisa.utils.JavaPropertiesUtils.getPropertyOrThrowError;
import static java.util.Collections.emptyList;

public class SubRunner implements Runnable {
    private final static Logger logger = Logger.getLogger(SubRunner.class.getName());
    private URI proxyEndpoint;
    private MLClusterNode thisNode;

    private SubController controller;
    private SubWsClient sender;

    public SubRunner(Properties properties) {
        loadProperties(properties);
    }

    public void loadProperties(Properties prop) {
        final String proxyUrlValue = getPropertyOrThrowError(prop, PROXY_ENDPOINT.getPropertyKey());
        final String subscriberName = getPropertyOrThrowError(prop, SUBSCRIBER_NAME.getPropertyKey());
        final String subscriberHostname = getPropertyOrThrowError(prop, SUBSCRIBER_HOSTNAME.getPropertyKey());

        try {
            proxyEndpoint = new URI(proxyUrlValue);
        } catch (URISyntaxException e) {
            throw new RuntimeException("Invalid proxy URI: " + proxyUrlValue, e);
        }

        this.thisNode = new MLClusterNode(subscriberName, subscriberHostname, Set.of(BINARY_CLIENT));
    }

    private void sendMessageToProxy(BaseMessage message) {
        if (sender == null) {
            logger.warning("Tried to send a message to proxy but no sender available");
            return;
        }

        sender.getWsProxyClientBus().offer(message);
    }

    @Override
    public void run() {
        this.controller = new SubController(thisNode, this::sendMessageToProxy);
        this.sender = new SubWsClient(proxyEndpoint, controller.getControllerBus());

        final String trackId = "initial-registration-" + thisNode.getNodeId();
        final RegistrationMessage initialMessage = new RegistrationMessage(trackId, thisNode);
        controller.inboundMessage(new MessageEnvelope(initialMessage, baseMessage -> {}));
    }

    public static void main(String[] args) throws IOException {
        // proxy config file setup
        String subConfigFile = System.getProperty("SUB_CONFIG_FILE");
        if (subConfigFile == null) {
            subConfigFile = "src/main/resources/default-sub.properties";
        }

        // proxy setup
        final Properties testingSubProperties = new Properties();
        testingSubProperties.load(new FileInputStream(subConfigFile));
        final SubRunner subRunner = new SubRunner(testingSubProperties);

        subRunner.run();

        final monPredicate allPred = new monPredicate("*", "*", "*", 0, Long.MAX_VALUE, new String[]{"*"}, new String[]{});
        final SubscribeMessage subscribeMessage = new SubscribeMessage(UUID.randomUUID().toString(), subRunner.thisNode, List.of(allPred), emptyList());
        final Consumer<BaseMessage> baseMessageConsumer = baseMessage -> {
//            logger.info("Got a message: " + baseMessage);
        };
        subRunner.controller.inboundMessage(new MessageEnvelope(subscribeMessage, baseMessageConsumer));

//        final DiscoveryMessage discoveryMessage = new DiscoveryMessage("a-random-message-track-id", subRunner.thisNode, MLTreeLevel.MODULE);
//        final Consumer<BaseMessage> baseMessageConsumer = baseMessage -> {
//            logger.info("Got a message: " + baseMessage);
//        };
//        subRunner.controller.inboundMessage(new MessageEnvelope(discoveryMessage, baseMessageConsumer));
    }
}
