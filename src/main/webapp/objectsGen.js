function generateRandomId(length) {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let id = '';
    for (let i = 0; i < length; i++) {
        id += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return id;
}

function getNode(nodeId) {
    return {
        "nodeId" : nodeId,
        "nodeUri" : "localhost",
        "capabilities" : [
            "WEB_CLIENT"
        ]
    }
}

function generateRegistrationMessage(nodeId) {
    var registrationMessage = {
        "trackId" : nodeId + "-registration",
        "node" : getNode(nodeId),
        "type" : "registration-message-class"
    }

    return registrationMessage;
}

function generateSubscribeMessage(trackId, node) {
    return {
        "messageTrackId" : trackId,
        "originNode" : node,
        "addPredicates" : [
            {
                "Farm" : "*",
                "Cluster" : "*",
                "Node" : "*",
                "tmin" : 0,
                "tmax" : -1,
            }
        ],
        "deletePredicates" : [],
        "type" : "subscribe-message-class"
    }
}

function parseDataResponse(jsonData) {
    parsedData = JSON.parse(jsonData);
    const events = []
    for (let i = 0; i < parsedData.data[0].param_name.length; i++) {
        var event = {
            "node" : [
                {
                    "Farm" : parsedData.data[0].FarmName,
                    "Cluster" : parsedData.data[0].ClusterName,
                    "Node" : parsedData.data[0].NodeName,
                    "Load" : parsedData.data[0].param_name[i]
                }
            ],
            "data" : [
                {
                    "time" : new Date(parsedData.data[0].time).toISOString(),
                    "value" : parsedData.data[0].param[i]
                }
            ]
        }
        events.push(event);
    }

    var newData = {
        "type" : "subscribe",
        "id" : parsedData.messageTrackId,
        "events" : events
    }

    return newData;
}

function generateNewData() {

    var newValue = Math.floor(Math.random() * 1000); // Example: generate a random value

    // Create a new data object with the same "node" structure but with the new value
    var newData = {
        "type": "subscribe",
        "id": "uuid-" + Math.random().toString(36).substr(2, 9), // Generate a pseudo-random UUID for demonstration
        "events": [
            {
                "node" : [
                    {
                        "Farm": "Farm",
                        "Cluster": "Cluster",
                        "Node": "Node",
                        "Load": "Load1"
                    }
                ],
                "data": [
                    {
                        "time": new Date().toISOString(),
                        "value": newValue
                    }
                ]
            }
        ]
    };

    return newData;
}

function generateNewData2() {

    var newValue = Math.floor(Math.random() * 1000); // Example: generate a random value

    // Create a new data object with the same "node" structure but with the new value
    var newData = {
        "type": "subscribe",
        "id": "uuid-" + Math.random().toString(36).substr(2, 9), // Generate a pseudo-random UUID for demonstration
        "events": [
            {
                "node" : [
                    {
                        "Farm": "Farm",
                        "Cluster": "Cluster",
                        "Node": "Node",
                        "Load": "Load2"
                    }
                ],
                "data": [
                    {
                        "time": new Date().toISOString(),
                        "value": newValue
                    }
                ]
            }
        ]
    };

    return newData;
}

function generateNewData3() {

    var newValue = Math.floor(Math.random() * 1000); // Example: generate a random value

    // Create a new data object with the same "node" structure but with the new value
    var newData = {
        "type": "subscribe",
        "id": "uuid-" + Math.random().toString(36).substr(2, 9), // Generate a pseudo-random UUID for demonstration
        "events": [
            {
                "node" : [
                    {
                        "Farm": "Farm",
                        "Cluster": "Cluster",
                        "Node": "Node2",
                        "Load": "Load2"
                    }
                ],
                "data": [
                    {
                        "time": new Date().toISOString(),
                        "value": newValue
                    }
                ]
            }
        ]
    };

    return newData;
}