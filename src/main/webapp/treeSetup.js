function addDataToStructures(newDataElement, dataMap) {
    if (newDataElement.events && Array.isArray(newDataElement.events)) {
        newDataElement.events.forEach(event => {
            let nodeItem = event.node[0]; // Access the first (and only) node object
            let dataItem = event.data[0]; // Access the first (and only) data object
            let path = [nodeItem.Farm, nodeItem.Cluster, nodeItem.Node];
            let currentNode = null;
            let name;
            let nodeId = `${nodeItem.Farm}-${nodeItem.Cluster}-${nodeItem.Node}`;
            let parentId = "#";
            let isNode = 0;

            path.forEach((key, index) => {
                if (index === 0) {
                    name = key;
                }
                if (index === 1) {
                    key = `${nodeItem.Farm}-${nodeItem.Cluster}`;
                    name = `${nodeItem.Cluster}`;
                    parentId = `${nodeItem.Farm}`;
                } else if (index === 2) {
                    key = `${nodeItem.Farm}-${nodeItem.Cluster}-${nodeItem.Node}`;
                    name = `${nodeItem.Node}`;
                    parentId = `${nodeItem.Farm}-${nodeItem.Cluster}`;
                    isNode = 1;
                }
                if (!nodeMap[key]) {
                    let newNode = {
                        id: key,
                        text: name,
                        isNode: isNode,
                        loads: {},
                        parentId: parentId
                    };
                    $('#SimpleJSTree').jstree("create_node", parentId, newNode, "last", null, false);
                    nodeMap[key] = newNode;
                }
                // function that automatically adds nodes in the tree
                currentNode = $('#SimpleJSTree').jstree(true).get_node(key);

                if (currentNode.original.isNode === 1 && nodeItem?.Load) {

                    if (!currentNode.original.loads[nodeItem.Load]) {
                        currentNode.original.loads[nodeItem.Load] = [];
                    }

                    let uniqueId = `${nodeItem.Farm}-${nodeItem.Cluster}-${nodeItem.Node}-${nodeItem.Load}`;
                    if (!dataMap[uniqueId]) {
                        dataMap[uniqueId] = [dataItem];
                    } else {
                        //TODO: to be only the values for the last 2 hours
                        if (dataMap[uniqueId].length >= 50) {
                            dataMap[uniqueId].shift();
                        }
                        dataMap[uniqueId].push(dataItem);
                    }

                    if (currentNode.original.loads[nodeItem.Load].length >= 50) {
                        currentNode.original.loads[nodeItem.Load].shift(); // Removes the oldest element
                    }

                    currentNode.original.loads[nodeItem.Load].push(dataItem);
                    // Sorting by date to ensure chronological order
                    currentNode.original.loads[nodeItem.Load].sort((a, b) => new Date(a.time) - new Date(b.time));

                }
            });
        });
    }
}
function createJSTree() {
    $('#SimpleJSTree').jstree({
        'core': {
            'data': treeData,
            'check_callback': true
        },
        'plugins': ["search", "contextmenu", "state"],
        'contextmenu': {
            'items': function (node) {
                return {
                    "Expand": {
                        label: "Expand",
                        action: function () {
                            $("#SimpleJSTree").jstree(true).open_node(node);
                        }
                    },
                    "Collapse": {
                        label: "Collapse",
                        action: function () {
                            $("#SimpleJSTree").jstree(true).close_node(node);
                        }
                    }
                };
            }
        },
        'state' : {
            "key" : "state_key"
        }
    });
}