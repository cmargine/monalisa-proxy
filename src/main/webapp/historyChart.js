function historyChart() {
    if (selectParamHistoryChart.length === 0) {
        alert("Niciun parametru selectat. Selectați cel puțin un parametru pentru a genera graficul.");
        return;
    }
    var uniqueId = new Date().getTime();
    var modalId = 'chartModal-' + uniqueId;
    var canvasId = 'myChart-' + uniqueId;
    var newButtonId = 'newButtonId-' + uniqueId;
    var ZoomId = 'ZoomId-' + uniqueId;
    var ZoomId2 = 'ZoomId2-' + uniqueId;
    var Init = 'Init-' + uniqueId;

    var modalHTML = `
<div class="modal fade" id="${modalId}" tabindex="-1" role="dialog" aria-labelledby="${modalId}Label">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="${modalId}Label" style="margin-right: 20px;">History Chart</h5>
                <div class="btn-toolbar" role="toolbar">
                    <!-- Dropdown Menu -->
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" style="background-color: #007bff;" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Menu
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <button class="dropdown-item" id="${newButtonId}">Select Time</button>
                            <button class="dropdown-item" id="${ZoomId}">Zoom</button>
                            <button class="dropdown-item" id="${ZoomId2}">SelectZoom</button>
                            <button class="dropdown-item" id="${Init}">Init</button>
                        </div>
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <canvas id="${canvasId}" width="200" height="300"></canvas>
            </div>
        </div>
    </div>
</div>`;

    $('body').append(modalHTML);

    $('#' + modalId).modal({
        show: true,
        backdrop: 'static',
        keyboard: false
    });

    $('#' + modalId).on('shown.bs.modal', function () {
        $(this).draggable({
            handle: ".modal-header"
        });

        var modalInstance = $(this);
        historyChartParameters[canvasId] = selectParamHistoryChart.slice();
        changeChart[canvasId] = 0;

        $('#' + newButtonId).off('click').on('click', function() {
            $('#timeSelectionModal').modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        $('#startTimeOptions').off('click').on('click', '.list-group-item', function() {
            var selectedTimeText = $(this).text();
            var selectedMinutes = parseInt(selectedTimeText.split(' ')[0]);
            startTime = moment().subtract(selectedMinutes, 'minutes');

            if (endTime) {
                changeChart[canvasId] = 1;
                filterAndUpdateChart();
            }
        });

        $('#endTimeOptions').off('click').on('click', '.list-group-item', function() {
            var selectedTimeText = $(this).text();
            var selectedMinutes = parseInt(selectedTimeText.split(' ')[0]);
            endTime = moment().subtract(selectedMinutes, 'minutes');
            // console.log("End Time: " + endTime.toISOString());

            if (startTime) {
                changeChart[canvasId] = 1;
                filterAndUpdateChart();
            }
        });

        function filterAndUpdateChart() {
            var chartInstance = historyChartInstances[canvasId];
            const chartParams = historyChartParameters[canvasId];
            if (!chartParams) {
                console.error("No parameters found for chart:", canvasId);
                return;
            }

            let allTimestamps = [];
            chartParams.forEach(param => {
                const dataParam = dataMap[param.id];
                allTimestamps = allTimestamps.concat(dataParam.map(item => moment(item.time).toISOString()));
            });

            allTimestamps = [...new Set(allTimestamps)].sort();

            if (chartInstance) {
                var desiredStartTime = startTime.toDate();
                var desiredEndTime = endTime.toDate();

                var datasets = chartInstance.data.datasets;
                var pointsInRange = datasets.flatMap(dataset => dataset.data.filter(point =>
                    point.x >= desiredStartTime && point.x <= desiredEndTime
                ));

                var minDate = pointsInRange.length > 0 ? pointsInRange.reduce((min, p) => p.x < min ? p.x : min, pointsInRange[0].x) : desiredStartTime;
                var maxDate = pointsInRange.length > 0 ? pointsInRange.reduce((max, p) => p.x > max ? p.x : max, pointsInRange[0].x) : desiredEndTime;

                var padding = (maxDate - minDate) * 0.1; // 10% padding
                chartInstance.options.scales.x.min = new Date(minDate - padding);
                chartInstance.options.scales.x.max = new Date(maxDate + padding);
                chartInstance.update('none');
            }
            $('#timeSelectionModal').modal('hide');
        }

        modalInstance.find('#' + Init).off('click').on('click', function() {
            changeChart[canvasId] = 0;
            var chart = historyChartInstances[canvasId];
            if (chart) {
                if (chart.options.scales.x) {
                    chart.options.scales.x.min = undefined;
                    chart.options.scales.x.max = undefined;
                }
                if (chart.options.scales.y) {
                    chart.options.scales.y.min = undefined;
                    chart.options.scales.y.max = undefined;
                }
                startTime = endTime = null;
                chart.update();
            }
        });

        modalInstance.find('#' + ZoomId).off('click').on('click', function() {
            var chart = historyChartInstances[canvasId];
            changeChart[canvasId] = 1;
            if (chart) {
                var zoomEnabled = chart.options.plugins.zoom.zoom.wheel.enabled;
                chart.options.plugins.zoom.zoom.wheel.enabled = !zoomEnabled;
                startTime = endTime = null;
                chart.update();
            }
        });

        modalInstance.find('#' + ZoomId2).off('click').on('click', function() {
            changeChart[canvasId] = 1;
            var chart = historyChartInstances[canvasId];
            if (chart) {
                window.rectangleZoomEnabled = !window.rectangleZoomEnabled;

                if (window.rectangleZoomEnabled) {
                    startTime = endTime = null;
                    var selectionCanvas = $('#' + canvasId).siblings('.selection-canvas')[0];
                    if (!selectionCanvas) {
                        selectionCanvas = $('<canvas class="selection-canvas" style="position: absolute; top: 0; left: 0; pointer-events: none;"></canvas>');
                        $('#' + canvasId).parent().css('position', 'relative').append(selectionCanvas);
                        selectionCanvas = selectionCanvas[0];
                    }
                    selectionCanvas.width = $('#' + canvasId).width();
                    selectionCanvas.height = $('#' + canvasId).height();
                    var ctx = selectionCanvas.getContext('2d');

                    $('#' + canvasId).on('mousedown', function(event) {
                        var startPoint = { x: event.offsetX, y: event.offsetY };

                        $(this).on('mousemove', function(event) {
                            ctx.clearRect(0, 0, selectionCanvas.width, selectionCanvas.height);

                            var width = event.offsetX - startPoint.x;
                            var height = event.offsetY - startPoint.y;

                            ctx.beginPath();
                            ctx.rect(startPoint.x, startPoint.y, width, height);
                            ctx.strokeStyle = 'rgba(0, 123, 255, 0.8)';
                            ctx.stroke();
                            ctx.closePath();
                        });
                        $(this).on('mouseup', function(event) {
                            $(this).off('mousemove mouseup');
                            ctx.clearRect(0, 0, selectionCanvas.width, selectionCanvas.height);

                            var endPoint = { x: event.offsetX, y: event.offsetY };

                            var xMin = Math.min(startPoint.x, endPoint.x);
                            var xMax = Math.max(startPoint.x, endPoint.x);
                            var yMin = Math.min(startPoint.y, endPoint.y);
                            var yMax = Math.max(startPoint.y, endPoint.y);

                            var xMinValue = chart.scales['x'].getValueForPixel(xMin);
                            var xMaxValue = chart.scales['x'].getValueForPixel(xMax);
                            var yMinValue = chart.scales['y'].getValueForPixel(yMax);
                            var yMaxValue = chart.scales['y'].getValueForPixel(yMin);

                            chart.options.scales.x.min = xMinValue;
                            chart.options.scales.x.max = xMaxValue;
                            chart.options.scales.y.min = yMinValue;
                            chart.options.scales.y.max = yMaxValue;

                            chart.update();

                            window.rectangleZoomEnabled = false;
                            window.rectangleZoomEnabled = false;
                            $('#' + canvasId).removeClass('crosshair-cursor');
                        });
                    });
                } else {
                    $('#' + canvasId).removeClass('crosshair-cursor');
                    $('#' + canvasId).off('mousedown mousemove mouseup');
                }
            }
        });

        var labels = selectParamHistoryChart.length > 0
            ? selectParamHistoryChart[0].data.map(item => moment(item.time).format('YYYY-MM-DD HH:mm:ss'))
            : [];

        var datasets = selectParamHistoryChart.map(function(leaf, index) {
            var data = leaf.data.map(item => item.value);
            return {
                label: leaf.text,
                data: data,
                backgroundColor: `rgba(${(index * 30) % 255}, ${(123 + index * 50) % 255}, ${(255 - index * 20) % 255}, 0.5)`,
                borderColor: `rgba(${(index * 30) % 255}, ${(123 + index * 50) % 255}, ${(255 - index * 20) % 255}, 1)`,
                borderWidth: 2
            };
        });

        createChart(canvasId, {
            labels: labels,
            datasets: datasets
        });
        deselectAllParameters();
    });

    $('#' + modalId).on('hidden.bs.modal', function () {
        selectParamHistoryChart = [];
        delete historyChartParameters[canvasId];
        $(this).remove();
    });
}

function createChart(canvasId, chartData) {
    var ctx = document.getElementById(canvasId).getContext('2d');
    const { datasets } = gatherHistoricalDataForChart(canvasId, dataMap);
    const firstTimestamp = datasets.length > 0 ? datasets[0].data[0].x : undefined;
    const lastTimestamp = datasets.length > 0 ? datasets[datasets.length - 1].data[datasets[datasets.length - 1].data.length - 1].x : undefined;

    var chartInstance = new Chart(ctx, {
        type: 'line',
        data: {
            labels: chartData.labels,
            datasets: datasets
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            animation: {
                duration: 0
            },
            scales: {
                y: {
                    beginAtZero: true,
                },
                x: {
                    type: 'time',
                    time: {
                        unit: 'minute',
                        tooltipFormat: 'll HH:mm',
                        displayFormats: {
                            minute: 'HH:mm',
                            hour: 'MMM D, HH:mm'
                        }
                    },
                    ticks: {
                        source: 'data',
                        autoSkip: false,
                        maxTicksLimit: 20,
                        min: firstTimestamp,
                        max: lastTimestamp
                    },
                    min: firstTimestamp,
                    max: lastTimestamp
                }
            },
            zoom: {
                pan: {
                    enabled: true,
                    mode: 'xy'
                },
                zoom: {
                    wheel: {
                        enabled: true,
                    },
                    pinch: {
                        enabled: true
                    },
                    mode: 'xy',
                    limits: {
                        max: 10,
                        min: 0.5
                    }
                }
            }
        }
    });
    historyChartInstances[canvasId] = chartInstance;
}

function gatherHistoricalDataForChart(canvasId, dataMap) {
    const chartParams = historyChartParameters[canvasId];
    if (!chartParams) {
        console.error("No parameters found for chart:", canvasId);
        return;
    }

    let allTimestamps = [];
    chartParams.forEach(param => {
        const dataParam = dataMap[param.id];
        allTimestamps = allTimestamps.concat(dataParam.map(item => moment(item.time).toISOString()));
    });
    allTimestamps = [...new Set(allTimestamps)].sort();

    let datasets = chartParams.map((param, index) => {
        const paramId = param.id;
        const dataParam = dataMap[paramId];

        const timestampValueMap = dataParam.reduce((map, item) => {
            map[moment(item.time).toISOString()] = item.value;
            return map;
        }, {});

        const backgroundColor = `rgba(${(index * 30) % 255}, ${(123 + index * 50) % 255}, ${(255 - index * 20) % 255}, 0.5)`;
        const borderColor = `rgba(${(index * 30) % 255}, ${(123 + index * 50) % 255}, ${(255 - index * 20) % 255}, 1)`;

        const dataPoints = allTimestamps.map(timestamp => ({
            x: timestamp,
            y: timestampValueMap.hasOwnProperty(timestamp) ? timestampValueMap[timestamp] : null
        }));

        return {
            label: param.text,
            data: dataPoints,
            spanGaps: true,
            backgroundColor: backgroundColor,
            borderColor: borderColor,
            borderWidth: 2
        };
    });

    return { datasets };
}

function updateAllChartsWithHistoryData(dataMap) {
    Object.keys(historyChartParameters).forEach(canvasId => {
        const datasets = gatherHistoricalDataForChart(canvasId, dataMap).datasets;
        if (datasets && changeChart[canvasId] === 0) {
            updateHistoryChartData(canvasId, datasets);
        }
    });
}

function updateHistoryChartData(canvasId, newDatasets) {
    var chartInstance = historyChartInstances[canvasId];
    if (!chartInstance) {
        console.error('Chart instance not found for canvas ID:', canvasId);
        return;
    }

    const labels = newDatasets.length > 0
        ? newDatasets[0].data.map(data => moment(data.x).format('YYYY-MM-DD HH:mm:ss'))
        : [];

    chartInstance.data.labels = labels;
    chartInstance.data.datasets = newDatasets;

    let maxYValue = 0;
    newDatasets.forEach(dataset => {
        dataset.data.forEach(point => {
            if (point.y != null && point.y > maxYValue) {
                maxYValue = point.y;
            }
        });
    });

    const currentMaxY = chartInstance.options.scales.y.max || 0;
    if (maxYValue > currentMaxY) {
        chartInstance.options.scales.y.max = maxYValue;
    }

    const minDate = new Date(Math.min(...newDatasets.map(ds => new Date(ds.data[0].x))));
    const maxDate = new Date(Math.max(...newDatasets.map(ds => new Date(ds.data[ds.data.length - 1].x))));
    chartInstance.options.scales.x.min = minDate.toISOString();
    chartInstance.options.scales.x.max = maxDate.toISOString();

    chartInstance.update();
}