function lastValueChart() {
    var uniqueId = new Date().getTime();
    var modalId = 'customChartModal-' + uniqueId;
    var canvasId = 'customChart-' + uniqueId;

    var modalHTML = `
<div class="modal fade" id="${modalId}" tabindex="-1" role="dialog" aria-labelledby="${modalId}Label">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="${modalId}Label">Last Value Chart</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <canvas id="${canvasId}" width="100" height="200"></canvas>
            </div>
        </div>
    </div>
</div>`;

    $('body').append(modalHTML);

    $('#' + modalId).modal({
        show: true,
        backdrop: 'static',
        keyboard: false
    });

    $('#' + modalId).on('shown.bs.modal', function () {
        var labels = selectParamLVChart.map(leaf => leaf.text);
        var data = selectParamLVChart.map(leaf => {
            // Obține ultima valoare (leaf) din fiecare load selectat
            var lastLeaf = leaf.data[leaf.data.length - 1] || {};
            return lastLeaf.value || null;
        });
        lastValueChartParameters[canvasId] = selectParamLVChart.slice();

        var datasets = [{
            label: 'Last Value',
            data: data,
            backgroundColor: 'rgba(0, 123, 255, 0.5)',
            borderColor: 'rgba(0, 123, 255, 1)',
            borderWidth: 2
        }];

        createBarChart(canvasId, {
            labels: labels,
            datasets: datasets
        });

        $('#' + modalId).modal('show');
        $(this).draggable({
            handle: ".modal-header"
        });
        deselectAllParameters();
    });


    $('#' + modalId).on('hidden.bs.modal', function () {
        selectParamLVChart = [];
        delete lastValueChartParameters[canvasId];
        $(this).remove();
    });
}

function createBarChart(canvasId, chartData) {
    var ctx = document.getElementById(canvasId).getContext('2d');
    var chartInstance = new Chart(ctx, {
        type: 'bar',
        data: chartData,
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
    lastValueChartInstances[canvasId] = chartInstance;
}


function gatherLatestDataForChart(canvasId, dataMap) {
    const chartParams = lastValueChartParameters[canvasId];

    if (!chartParams) {
        console.error("No parameters found for chart:", canvasId);
        return;
    }

    let newLabels = [];
    let newDataValues = [];

    chartParams.forEach(param => {
        const paramId = param.id;
        const dataParam = dataMap[paramId];

        if (dataParam && dataParam.length > 0) {
            const latestData = dataParam[dataParam.length - 1];
            newLabels.push(param.text);
            newDataValues.push(latestData.value);
        }
    });

    return { newLabels, newDataValues };
}

function updateAllChartsWithLatestData(dataMap) {
    Object.keys(lastValueChartParameters).forEach(canvasId => {
        const { newLabels, newDataValues } = gatherLatestDataForChart(canvasId, dataMap);
        if (newLabels && newDataValues) {
            updateChartData(canvasId, newLabels, newDataValues);
        }
    });
}

function updateChartData(canvasId, newLabels, newData) {
    var chartInstance = lastValueChartInstances[canvasId];
    if (!chartInstance) {
        console.error('Chart instance not found:', canvasId);
        return;
    }
    // Update chart data
    chartInstance.data.labels = newLabels;
    chartInstance.data.datasets.forEach((dataset) => {
        dataset.data = newData;
    });
    chartInstance.update();
}