plugins {
    id("java")
}

group = "alien.monalisa"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation("org.apache.tomcat:tomcat-websocket:10.1.5")
    implementation("org.apache.tomcat:tomcat-catalina:10.1.5")
//    implementation("com.esotericsoftware:kryo:5.5.0")
    implementation("de.ruedigermoeller:fst:2.57")

    implementation("com.fasterxml.jackson.core:jackson-core:2.17.0")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.17.0")
    implementation("com.fasterxml.jackson.core:jackson-annotations:2.17.0")
}

tasks.register<Jar>("jarProxy") {
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    manifest {
        attributes(
            "Main-Class" to "alien.monalisa.proxy.ProxyRunner"
        )
    }
    from(sourceSets.main.get().output)
    from(sourceSets.main.get().resources)
    from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
    archiveBaseName.set("proxy")
}

tasks.register<Jar>("jarFarm") {
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    manifest {
        attributes(
            "Main-Class" to "alien.monalisa.farm.FarmRunner"
        )
    }
    from(sourceSets.main.get().output)
    from(sourceSets.main.get().resources)
    from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
    archiveBaseName.set("farm")
}

tasks.register<Jar>("jarSub") {
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    manifest {
        attributes(
            "Main-Class" to "alien.monalisa.sub.SubRunner"
        )
    }
    from(sourceSets.main.get().output)
    from(sourceSets.main.get().resources)
    from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
    archiveBaseName.set("sub")
}

val bundleZip by tasks.registering(Zip::class) {
    // Define the name and location of the output zip file
    archiveFileName.set("bundle.zip")
    destinationDirectory.set(layout.buildDirectory.dir("distributions"))

    // Include jar files from the build outputs
    from(layout.buildDirectory.dir("libs")) {
        include("*.jar")
    }

    // Include an additional directory
    from("src/main/webapp") {
        into("webapp") // Place this directory within a 'config' folder inside the zip
    }
}

tasks.named("build") {
    dependsOn("jarProxy", "jarFarm", "jarSub")
    finalizedBy(bundleZip)
}
